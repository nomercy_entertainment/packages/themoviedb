<?php

/*
 * You can place your custom package configuration in here.
 */
return [

    // Download images from the TheMovieDB.
    'download_images' => false, // Defaults to CDN from themoviedb.org

    // Download trailers from the Youtube.
    'download_trailers' => false, // Defaults to embedded video.

    // Set the minimum amount of days before the shows update the database.
    'not_update_before' => 4,

    // Set the default language for the translated items.
    'set_language' => null,  // Defaults to the browser requested langueage, fallback: english,

    // Set the default Country in iso_3166_1 format you want to recieve data for.
    'set_country' => null, // Defauls to cloudflare-country, Fallback: US.

    'default_language' => 'en',

];
