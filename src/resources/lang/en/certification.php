<?php

return [

    'NL' => [
        "6" => "Mogelijk schadelijk tot 6.",
        "9" => "Mogelijk schadelijk tot 9.",
        "12" => "Mogelijk schadelijk tot 12.",
        "16" => "Mogelijk schadelijk tot 16.",
        "NR" => "Geen classificatie.",
        "AL" => "Alle leeftijden."
    ],


    "US" => [
        "TV-Y7" => "This program is designed for children age 7 and above.",
        "TV-G" => "Most parents would find this program suitable for all ages.",
        "TV-14" => "This program contains some material that many parents would find unsuitable for children under 14 years of age.",
        "NR" => "No rating information.",
        "TV-PG" => "This program contains material that parents may find unsuitable for younger children.",
        "TV-Y" => "This program is designed to be appropriate for all children.",
        "TV-MA" => "This program is specifically designed to be viewed by adults and therefore may be unsuitable for children under 17."
    ],
    "CA" => [
        "18+" => "Programming intended for viewers ages 18 and older. May contain explicit violence and sexual activity. Programming with this rating cannot air before the watershed (9 =>00 p.m. to 6 =>00 a.m.).",
        "PG" => "Parental guidance. Moderate violence and moderate profanity is allowed, as is brief nudity and sexual references if important to the context of the story.",
        "14+" => "Programming intended for viewers ages 14 and older. May contain strong violence and strong profanity, and depictions of sexual activity as long as they are within the context of a story.",
        "G" => "Suitable for general audiences. Programming suitable for the entire family with mild violence, and mild profanity and/or censored language.",
        "Exempt" => "Shows which are exempt from ratings (such as news and sports programming) will not display an on-screen rating at all.",
        "C8" => "Suitable for children ages 8+. Low level violence and fantasy horror is allowed. No foul language is allowed, but occasional \"socially offensive and discriminatory\" language is allowed if in the context of the story. No sexual content of any level allowed.",
        "C" => "Programming suitable for children ages of 2–7 years. No profanity or sexual content of any level allowed. Contains little violence."
    ],
    "AU" => [
        "PG" => "Parental guidance is recommended for young viewers; PG-rated content may air at any time on digital-only channels, otherwise, it should only be broadcast between 8 =>30 a.m. and 4 =>00 p.m. and between 7 =>00 p.m. and 6 =>00 a.m. on weekdays, and between 10 =>00 a.m. and 6 =>00 a.m. on weekends.",
        "MA15+" => "Not suitable for children and teens under 15; MA15+-rated programming may only be broadcast between 9 =>00 p.m. and 5 =>00 a.m. on any given day. Consumer advice is mandatory. Some R18+ rated movies on DVD/Blu-ray are often re-edited on free TV/cable channels to secure a more \"appropriate\" MA15+ rating. Some movies that were rated R18 on DVD have since been aired in Australian TV with MA15+ rating.",
        "R18+" => "Not for children under 18; this is limited to Adult \"Pay Per View\" VC 196 and 197. Content may include graphic violence, sexual situations, coarse language and explicit drug use.",
        "P" => "Programming is intended for younger children 2–11; commercial stations must show at least 30 minutes of P-rated content each weekday and weekends at all times. No advertisements may be shown during P-rated programs.",
        "M" => "Recommended for mature audiences; M-rated content may only be broadcast between 8 =>30 p.m. and 5 =>00 a.m. on any day, and additionally between 12 =>00 p.m. and 3 =>00 p.m. on schooldays.",
        "AV15+" => "Not suitable for children and teens under 15; this is the same as the MA15+ rating, except the \"AV\" stands for \"Adult Violence\" meaning that anything that is Classified \"MA15+\" with the consumer advice \"Frequent Violence\" or \"Strong Violence\" will automatically become AV15+ (with that same consumer advice.) The AV rating is still allowed to exceed any MA15+ content, in particular – 'Violence'. AV15+ content may only be broadcast between 9 =>30 p.m. and 5 =>00 a.m. on any day. Consumer advice is mandatory.",
        "G" => "For general exhibition; all ages are permitted to watch programming with this rating.",
        "C" => "Programming intended for older children 5–14; commercial stations must show at least 30 minutes of C-rated content each weekday between 7 a.m. and 8 a.m. or between 4 p.m. and 8 =>30 p.m. A further 2 and a half ours a week must also be shown either within these time bands or between 7 a.m. and 8 =>30 p.m. on weekends and school holidays, for a total of five hours a week (averaged as 260 hours over the course of a year). C-rated content is subject to certain restrictions and limitations on advertising (typically five minutes maximum per 30-minute period or seven minutes including promotions and community announcements)."
    ],
    "TH" => [
        "น 18+" => "Nor 18+ Movies appropriate for audiences aged 18 and older.",
        "น 13+" => "Nor 13+ Movies appropriate for audiences aged 13 and older.",
        "น 15+" => "Nor 15+ Movies appropriate for audiences aged 15 and older. Some violence, brutality, inhumanity, bad language or indecent gestures allowed.",
        "ส" => "Sor - Educational movies which the public should be encouraged to see.",
        "ท" => "Tor - G Movies appropriate for the general public. No sex, abusive language or violence.",
        "ฉ 20-" => "Chor 20 - Movies prohibited for audiences aged below 20.",
        "-" => "Banned."
    ],
    "KR" => [
        "7" => "This rating is for programming that may contain material inappropriate for children younger than 7, and parental discretion should be used. Some cartoon programming not deemed strictly as \"educational\", and films rated \"G\" or \"PG\" in North America may fall into the 7 category.",
        "12" => "This rating is for programs that may deemed inappropriate for those younger than 12, and parental discretion should be used. Usually used for animations that have stronger themes or violence then those designed for children, or for reality shows that have mild violence, themes, or language.",
        "15" => "This rating is for programs that contain material that may be inappropriate for children under 15, and that parental discretion should be used. Examples include most dramas, and talk shows on OTA (over-the-air) TV (KBS, MBC, SBS), and many American TV shows/dramas on Cable TV channels like OCN and OnStyle. The programs that have this rating may include moderate or strong adult themes, language, sexual inference, and violence. As with the TV-MA rating in North America, this rating is commonly applied to live events where the occurrence of inappropriate dialogue is unpredictable. Since 2007, this rating is the most used rating for TV.",
        "19" => "This rating is for programs that are intended for adults only. 19-rated programming cannot air during the hours of 7 =>00AM to 9 =>00AM, and 1 =>00PM to 10 =>00PM. Programmes that receive this rating will almost certainly have adult themes, sexual situations, frequent use of strong language and disturbing scenes of violence.",
        "ALL" => "This rating is for programming that is appropriate for all ages. This program usually involves programs designed for children or families.",
        "Exempt" => "This rating is only for knowledge based game shows; lifestyle shows; documentary shows; news; current topic discussion shows; education/culture shows; sports that excludes MMA or other violent sports; and other programs that Korea Communications Standards Commission recognizes."
    ],
    "PT" => [
        "16" => "Not suitable for children under 16, access to these programs is locked by a personal password.",
        "18" => "Not suitable for children under 18.",
        "12AP" => "Acompanhamento Parental (may not be suitable for children under 12, parental guidance advised).",
        "NR" => "No rating information.",
        "T" => "Todos (suitable for all).",
        "10AP" => "Acompanhamento Parental (may not be suitable for children under 10, parental guidance advised)."
    ],
    "PH" => [
        "G" => "Suitable for all ages. Material for television, which in the judgment of the Board does not contain anything unsuitable for children.",
        "X" => "Any television program that does not conform to the “G”, “PG”, and “SPG” classification shall be disapproved for television broadcast.",
        "SPG" => "Stronger and more vigilant parental guidance is suggested. Programs classified as “SPG” may contain more serious topic and theme, which may not be advisable for children to watch except under the very vigilant guidance and presence of a parent or an adult.",
        "NR" => "No rating information.",
        "PG" => "Parental guidance suggested. Material for television, which, in the judgment of the Board, may contain some adult material that may be permissible for children to watch but only under the guidance and supervision of a parent or adult."
    ],
    "DE" => [
        "0" => "Can be aired at any time.",
        "6" => "Can be aired at any time.",
        "12" => "The broadcaster must take the decision about the air time by taking in consideration the impact on young children in the timeframe from 6 =>00am to 8 =>00pm.",
        "16" => "Can be aired only from 10 =>00pm Uhr to 6 =>00am.",
        "18" => "Can be aired only from 11 =>00pm Uhr to 6 =>00am."
    ],
    "BR" => [
        "10" => "Content suitable for viewers over the age of 10.",
        "12" => "Content suitable for viewers over the age of 12.",
        "14" => "Content suitable for viewers over the age of 14.",
        "16" => "Content suitable for viewers over the age of 16.",
        "18" => "Content suitable for viewers over the age of 18.",
        "L" => "Content is suitable for all audiences."
    ],
    "CA-QC" => [
        "G" => "Appropriate for all ages and must contain little or no violence and little to no sexual content.",
        "8+" => "Appropriate for children 8 and up may contain with little violence, language, and little to no sexual situations.",
        "16+" => "Recommended for children over the age of 16 and may contain with strong violence, strong language, and strong sexual content.",
        "13+" => "Appropriate – suitable for children 13 and up and may contain with moderate violence, language, and some sexual situations.",
        "NR" => "No rating information.",
        "18+" => "Only to be viewed by adults and may contain extreme violence and graphic sexual content. It is mostly used for 18+ movies and pornography."
    ],
    "HU" => [
        "6" => "Programs not recommended for children below the age of 6, may not contain any violence or sexual content. A yellow circle with the number 6 written inside is used for this rating.",
        "12" => "Programs not recommended for children below the age of 12, may contain light sexual content or explicit language. Most films without serious violence or sexual content fit into this category as well. A yellow circle with the number 12 written inside is used for this rating.",
        "16" => "Programs not recommended for teens and children below the age of 16, may contain more intensive violence and sexual content. A yellow circle with the number 16 written inside is used for this rating.",
        "18" => "The program is recommended only for adult viewers (for ages 18 and up), may contain explicit violence and explicit sexual content. A red circle with the number 18 written inside is used for this rating (the red circle was also used until 2002, but it did not contain any number in it).",
        "Unrated" => "Without age restriction.",
        "Children" => "Programs recommended for children. It is an optional rating, there is no obligation for broadcasters to indicate it.",
        "NR" => "No rating information."
    ],
    "RU" => [
        "16+" => "Only teens the age of 16 or older can watch.",
        "18+" => "Restricted to People 18 or Older.",
        "6+" => "Only kids the age of 6 or older can watch.",
        "12+" => "Only kids the age of 12 or older can watch.",
        "0+" => "Can be watched by Any Age."
    ],
    "GB" => [
        "12" => "Films classified 12A and video works classified 12 contain material that is not generally suitable for children aged under 12.",
        "15" => "No-one under 15 is allowed to see a 15 film at the cinema or buy/rent a 15 rated video. 15 rated works are not suitable for children under 15 years of age.",
        "18" => "Films rated 18 are for adults. No-one under 18 is allowed to see an 18 film at the cinema or buy / rent an 18 rated video. No 18 rated works are suitable for children.",
        "PG" => "PG stands for Parental Guidance. This means a film is suitable for general viewing, but some scenes may be unsuitable for young children. A PG film should not unsettle a child aged around eight or older.",
        "12A" => "Films classified 12A and video works classified 12 contain material that is not generally suitable for children aged under 12. 12A requires an adult to accompany any child under 12 seeing a 12A film at the cinema.",
        "R18" => "The R18 category is a special and legally-restricted classification primarily for explicit works of consenting sex or strong fetish material involving adults.",
        "U" => "The U symbol stands for Universal. A U film should be suitable for audiences aged four years and over."
    ],
    "SK" => [
        "7" => "Content suitable for children over 6 years.",
        "12" => "Content suitable for children over 12 years.",
        "15" => "Content suitable for teens over 15 years.",
        "18" => "Content exclusively for adults.",
        "NR" => "No rating information."
    ],
    "ES" => [
        "7" => "Not recommended for viewers under the age of 7.",
        "10" => "Not recommended for viewers under the age of 10.",
        "12" => "Not recommended for viewers under the age of 12.",
        "13" => "Not recommended for viewers under the age of 13.",
        "16" => "Not recommended for viewers under the age of 16.",
        "18" => "Not recommended for viewers under the age of 18.",
        "TP" => "For general viewing.",
        "NR" => "No rating information.",
        "Infantil" => "Specially recommended for younger children."
    ],
    "LT" => [
        "NR" => "No rating information.",
        "N-7" => "Intended for viewers from 7 years old.",
        "N-14" => "Intended for viewers from 14 years of age and broadcast from 21 (9pm) to 6 (6am) hours.",
        "S" => "Intended for adult viewers from the age of 18 (corresponding to the age-appropriate index N-18) and broadcast between 23 (11pm) and 6 (6am) hours; Limited to minors and intended for adult audiences."
    ],
    "NL" => [
        "AL" => "Not harmful / All Ages.",
        "9" => "Take care with children under 9.",
        "16" => "Take care with children under 16.",
        "12" => "Take care with children under 12.",
        "6" => "Take care with children under 6.",
        "NR" => "No rating information."
    ],
    "FR" => [
        "10" => "Not recommended for children under 10. Not allowed in children's television series.",
        "12" => "Not recommended for children under 12. Not allowed air before 10 =>00 p.m. Some channels and programs are subject to exception.",
        "16" => "Not recommended for children under 16. Not allowed air before 10 =>30 p.m. Some channels and programs are subject to exception.",
        "18" => "Not recommended for persons under 18. Allowed between midnight and 5 a.m. and only in some channels, access to these programs is locked by a personal password.",
        "NR" => "No rating information."
    ]
];
