<?php

return [
    '404'                   => 'Sorry this content doesn\'t exist.',
    '401'                   => 'Sorry, you are forbidden from accessing this content.',
    '419'                   => 'Sorry, your session has expired. Please Login again.',
    '429'                   => 'Sorry, you are making too many requests to our servers.',
    '500'                   => 'Whoops, something went wrong on our servers.',

    '503'                   => 'Sorry, we are doing some maintenance. Please check back soon.',
    'mustLogin'             => 'You need to login to access this page.',
    'noAccess'              => 'You do not have permission to access this page access.',
    
    'tomannyitemsrequested' => 'The maximum amount 5',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',

];
