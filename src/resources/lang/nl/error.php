<?php

return [

    '404'                   => 'Sorry deze content bestaat niet.',
    '401'                   => 'Sorry, je heb niet de bevoegdheden voor deze content.',
    '419'                   => 'Sorry, je sessie is afgelopen. Log opnieuw in.',
    '429'                   => 'Help ik kan het niet meer aan... te veel drukte!!!',
    '500'                   => 'Whoops, er is iets mis gegaan met de server.',
    '503'                   => 'Sorry, wij zijn onderhoud aan het plegen. Probeer het straks nog eens.',

    
    'mustLogin'             => 'Voor deze content moet je ingeloged zijn.',
    'noAccess'              => 'Je heb geen toegang tot deze content.',

    'tomannyitemsrequested' => 'Het maximum aantal is 5',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',

];
