<?php

return [
    "US" => [
        "TV-Y7" => "Dit programma is bedoeld voor kinderen van 7 jaar en ouder.",
        "TV-G" => "De meeste ouders zouden dit programma geschikt vinden voor alle leeftijden.",
        "TV-14" => "Dit programma bevat materiaal dat veel ouders ongeschikt zouden vinden voor kinderen onder de 14 jaar.",
        "NR" => "Geen beoordelingsinformatie.",
        "TV-PG" => "Dit programma bevat materiaal dat ouders mogelijk niet geschikt vinden voor jongere kinderen.",
        "TV-Y" => "Dit programma is ontworpen om geschikt te zijn voor alle kinderen.",
        "TV-MA" => "Dit programma is speciaal ontworpen om door volwassenen bekeken te worden en is daarom mogelijk niet geschikt voor kinderen onder de 17 jaar."
    ],
    "CA" => [
        "18+" => "Programmering bedoeld voor kijkers van 18 jaar en ouder. Kan expliciet geweld en seksuele activiteit bevatten. Programmering met deze classificatie kan niet worden uitgezonden vóór de waterscheiding (9 => 00:00 uur tot 6:00 uur => 00:00 uur).",
        "PG" => "Ouderlijke begeleiding. Matig geweld en matige godslastering is toegestaan, evenals korte naaktheid en seksuele verwijzingen indien belangrijk voor de context van het verhaal.",
        "14+" => "Programmering bedoeld voor kijkers van 14 jaar en ouder. Kan gewelddadig en grof taalgebruik bevatten, en afbeeldingen van seksuele activiteit zolang ze binnen de context van een verhaal vallen.",
        "G" => "Geschikt voor een algemeen publiek. Programmering geschikt voor het hele gezin met licht geweld en milde godslastering en / of gecensureerd taalgebruik.",
        "Exempt" => "Shows die zijn vrijgesteld van beoordelingen (zoals nieuws en sportprogramma's), geven helemaal geen beoordeling op het scherm weer.",
        "C8" => "Geschikt voor kinderen van 8 jaar en ouder. Laag niveau geweld en fantasierijke horror is toegestaan. Grof taalgebruik is niet toegestaan, maar af en toe \"sociaal beledigend en discriminerend\" taalgebruik is toegestaan ​​indien in de context van het verhaal. Nee seksuele inhoud van elk niveau toegestaan. ",
        "C" => "Programmering geschikt voor kinderen van 2–7 jaar. Geen godslastering of seksuele inhoud van welk niveau dan ook. Bevat weinig geweld."
    ],
    "AU" => [
        "PG" => "Ouderlijk toezicht wordt aanbevolen voor jonge kijkers; content met een PG-classificatie kan op elk moment worden uitgezonden op kanalen die alleen digitaal zijn, anders mag het alleen worden uitgezonden tussen 8 => 30 uur en 16 uur => 00 uur en tussen 7 => 00 uur en 6 => 00 uur op weekdagen, en tussen 10 => 00 uur en 6 => 00 uur in het weekend. ",
        "MA15 +" => "Niet geschikt voor kinderen en tieners onder de 15; programma's met een MA15 + -classificatie mogen alleen op een bepaalde dag worden uitgezonden tussen 9 => 00:00 uur en 5 => 00:00 uur. Consumentenadvies is verplicht. Sommige films met een R18 + -classificatie op Dvd / Blu-ray worden vaak opnieuw bewerkt op gratis tv- / kabelkanalen om een ​​meer  \"geschikte\" MA15 + -classificatie te krijgen. Sommige films met een R18-classificatie op dvd zijn sindsdien uitgezonden op de Australische tv met een MA15 + -classificatie. ",
        "R18 +" => "Niet voor kinderen onder de 18; dit is beperkt tot Adult  \"Pay Per View\" VC 196 en 197. Inhoud kan expliciet geweld, seksuele situaties, grof taalgebruik en expliciet drugsgebruik bevatten.",
        "P" => "Programmeren is bedoeld voor jongere kinderen van 2–11; commerciële zenders moeten te allen tijde ten minste 30 minuten aan P-rated inhoud per weekdag en in het weekend vertonen. Er mogen geen advertenties worden getoond tijdens P-rated programma's.",
        "M" => "Aanbevolen voor een volwassen publiek; inhoud met een M-rating mag alleen worden uitgezonden tussen 8 => 30 uur en 5 => 00 uur op elke dag, en bovendien tussen 12 => 00 uur en 15 uur => 00 uur op schooldagen.",
        "AV15 +" => "Niet geschikt voor kinderen en tieners onder de 15; dit is hetzelfde als de MA15 + -classificatie, behalve dat de \"AV \"staat voor\"Geweld door volwassenen\", wat betekent dat alles dat geclassificeerd is als \"MA15 + \"met het consumentenadvies\"Frequent Violence \"of\"Strong Violence\" wordt automatisch AV15 + (met hetzelfde consumentenadvies). De AV-rating mag nog steeds elke MA15 + -inhoud overschrijden, in het bijzonder - 'Geweld'. AV15 + -inhoud mag elke dag alleen worden uitgezonden tussen 9 => 30 uur en 5 => 00 uur. Consumentenadvies is verplicht. ",
        "G" => "Voor algemene tentoonstellingen; alle leeftijden mogen naar programma's kijken met deze classificatie.",
        "C" => "Programmering bedoeld voor oudere kinderen van 5 t / m 14 jaar; commerciële zenders moeten elke weekdag tussen 7.00 en 8.00 uur of tussen 16.00 en 20.00 uur of tussen 16.00 en 20.00 uur ten minste 30 minuten aan C-rated content vertonen. de helft van ons per week moet ook worden vertoond, hetzij binnen deze tijdbanden, hetzij tussen 7 uur en 20 uur => 30 uur in het weekend en tijdens schoolvakanties, voor een totaal van vijf uur per week (gemiddeld 260 uur in de loop van een jaar) . Content met een C-rating is onderhevig aan bepaalde beperkingen en beperkingen voor advertenties (doorgaans maximaal vijf minuten per periode van 30 minuten of zeven minuten inclusief promoties en aankondigingen van de gemeenschap). "
    ],
    "TH" => [
        "น 18+" => "Noch 18+ films geschikt voor kijkers van 18 jaar en ouder.",
        "น 13+" => "Noch 13+ films geschikt voor kijkers van 13 jaar en ouder.",
        "น 15+" => "Noch 15+ films geschikt voor kijkers van 15 jaar en ouder. Enig geweld, brutaliteit, onmenselijkheid, grof taalgebruik of onfatsoenlijke gebaren zijn toegestaan.",
        "ส" => "Sor - Educatieve films waarvan het publiek zou moeten worden aangemoedigd om ze te zien.",
        "ท" => "Tor-G-films geschikt voor het grote publiek. Geen seks, grof taalgebruik of geweld.",
        "ฉ 20-" => "Chor 20 - Films verboden voor kijkers onder de 20 jaar.",
        "-" => "Verbannen."
    ],
    "KR" => [
        "7" => "Deze beoordeling is voor programma's die mogelijk materiaal bevatten dat niet geschikt is voor kinderen jonger dan 7, en er moet ouderlijk toezicht worden gebruikt. Sommige tekenfilms worden niet strikt als \" educatief\" beschouwd, en films met de beoordeling \"G \" of \"PG\" in Noord-Amerika kan in categorie 7 vallen. ",
        "12" => "Deze beoordeling is voor programma's die als ongepast kunnen worden beschouwd voor personen jonger dan 12, en er moet ouderlijk toezicht worden gebruikt. Meestal gebruikt voor animaties met sterkere thema's of meer geweld dan die voor kinderen, of voor realityshows die mild geweld, thema's of taalgebruik. ",
        "15" => "Deze beoordeling is voor programma's die materiaal bevatten dat mogelijk ongeschikt is voor kinderen onder de 15, en dat ouderlijk toezicht moet worden gebruikt. Voorbeelden zijn de meeste drama's en talkshows op OTA (over-the-air) TV ( KBS, MBC, SBS) en veel Amerikaanse tv-programma's / drama's op kabel-tv-kanalen zoals OCN en OnStyle. De programma's met deze beoordeling kunnen matige of sterke volwassen thema's, taalgebruik, seksuele gevolgtrekkingen en geweld bevatten. Net als bij de tv- MA-classificatie in Noord-Amerika, deze classificatie wordt gewoonlijk toegepast op live-evenementen waar het voorkomen van ongepaste dialogen onvoorspelbaar is. Sinds 2007 is deze classificatie de meest gebruikte classificatie voor tv. ",
        "19" => "Deze classificatie is voor programma's die alleen bedoeld zijn voor volwassenen. Programma's met een classificatie van 19 kunnen niet worden uitgezonden tijdens de uren van 7 => 00:00 tot 9 => 00:00 en 1 => 00:00 tot 10 => 00:00. Programma's die deze beoordeling krijgen, hebben vrijwel zeker thema's voor volwassenen, seksuele situaties, veelvuldig grof taalgebruik en verontrustende geweldsscènes. ",
        "ALL" => "Deze classificatie is voor programma's die geschikt zijn voor alle leeftijden. Dit programma omvat meestal programma's die zijn ontworpen voor kinderen of gezinnen.",
        "Exempt" => "Deze classificatie is alleen voor op kennis gebaseerde spelshows; lifestyle-shows; documentaireshows; nieuws; actuele discussieprogramma's; onderwijs- / cultuurshows; sport die MMA of andere gewelddadige sporten uitsluit; en andere programma's die voldoen aan de Korea Communications Standards Commissie erkent. "
    ],
    "PT" => [
        "16" => "Niet geschikt voor kinderen onder de 16, de toegang tot deze programma's is vergrendeld met een persoonlijk wachtwoord.",
        "18" => "Niet geschikt voor kinderen onder de 18 jaar.",
        "12AP" => "Acompanhamento Parental (mogelijk niet geschikt voor kinderen onder de 12 jaar, ouderlijke begeleiding wordt geadviseerd).",
        "NR" => "Geen beoordelingsinformatie.",
        "T" => "Todos (geschikt voor iedereen).",
        "10AP" => "Acompanhamento Parental (mogelijk niet geschikt voor kinderen jonger dan 10 jaar, ouderlijke begeleiding wordt geadviseerd)."
    ],
    "PH" => [
        "G" => "Geschikt voor alle leeftijden. Materiaal voor televisie, dat naar het oordeel van de Raad niets bevat dat ongeschikt is voor kinderen.",
        "X" => "Elk televisieprogramma dat niet voldoet aan de classificatie \"G\",\"PG\" en \"SPG\"zal worden afgekeurd voor televisie-uitzendingen.",
        "SPG" => "Sterkere en waakzamere ouderlijke begeleiding wordt aanbevolen. Programma's die zijn geclassificeerd als \"SPG\" kunnen een serieuzer onderwerp en thema bevatten, wat voor kinderen misschien niet aan te raden is om naar te kijken, behalve onder de zeer waakzame begeleiding en aanwezigheid van een ouder of een volwassene.",
        "NR" => "Geen beoordelingsinformatie.",
        "PG" => "Ouderlijk toezicht aanbevolen. Materiaal voor televisie, dat, naar het oordeel van de Raad, materiaal voor volwassenen kan bevatten dat toegestaan ​​is voor kinderen om naar te kijken, maar alleen onder begeleiding en toezicht van een ouder of volwassene."
    ],
    "DE" => [
        "0" => "Kan op elk moment worden uitgezonden.",
        "6" => "Kan op elk moment worden uitgezonden.",
        "12" => "De omroep moet bij de beslissing over de uitzendtijd rekening houden met de impact op jonge kinderen in de periode van 6 => 00:00 uur tot 8:00 uur => 00:00 uur.",
        "16" => "Kan alleen worden uitgezonden van 10 => 00 uur Uhr tot 6 => 00 uur.",
        "18" => "Kan alleen worden uitgezonden van 11 => 00 uur Uhr tot 6 => 00 uur."
    ],
    "BR" => [
        "10" => "Content die geschikt is voor kijkers ouder dan 10 jaar",
        "12" => "Content die geschikt is voor kijkers ouder dan 12 jaar",
        "14" => "Content die geschikt is voor kijkers ouder dan 14 jaar",
        "16" => "Inhoud geschikt voor kijkers ouder dan 16 jaar",
        "18" => "Content die geschikt is voor kijkers ouder dan 18 jaar.",
        "L" => "Inhoud is geschikt voor alle doelgroepen."
    ],
    "CA-QC" => [
        "G" => "Geschikt voor alle leeftijden en mag weinig of geen geweld en weinig tot geen seksuele inhoud bevatten.",
        "8+" => "Geschikt voor kinderen van 8 jaar en ouder kan bevatten met weinig geweld, taal en weinig tot geen seksuele situaties.",
        "16+" => "Aanbevolen voor kinderen ouder dan 16 jaar en kan krachtig geweld, grof taalgebruik en sterke seksuele inhoud bevatten.",
        "13+" => "Geschikt - geschikt voor kinderen van 13 jaar en ouder en kan matig geweld, taalgebruik en sommige seksuele situaties bevatten.",
        "NR" => "Geen beoordelingsinformatie.",
        "18+" => "Alleen te bekijken door volwassenen en kan extreem geweld en expliciete seksuele inhoud bevatten. Het wordt meestal gebruikt voor 18+ films en pornografie."
    ],
    "HU" => [
        "6" => "Programma's die niet worden aanbevolen voor kinderen onder de 6 jaar, mogen geen geweld of seksuele inhoud bevatten. Een gele cirkel met het cijfer 6 erin wordt gebruikt voor deze beoordeling.",
        "12" => "Programma's die niet worden aanbevolen voor kinderen onder de 12 jaar, kunnen lichte seksuele inhoud of expliciet taalgebruik bevatten. De meeste films zonder ernstig geweld of seksuele inhoud passen ook in deze categorie. Een gele cirkel met het cijfer 12 erin geschreven wordt gebruikt voor deze beoordeling. ",
        "16" => "Programma's die niet worden aanbevolen voor tieners en kinderen onder de 16 jaar, kunnen intensiever geweld en seksuele inhoud bevatten. Een gele cirkel met het nummer 16 erin wordt gebruikt voor deze beoordeling.",
        "18" => "Het programma wordt alleen aanbevolen voor volwassen kijkers (vanaf 18 jaar), kan expliciet geweld en expliciete seksuele inhoud bevatten. Een rode cirkel met het cijfer 18 erin wordt gebruikt voor deze beoordeling (de rode cirkel was ook gebruikt tot 2002, maar er stond geen nummer in). ",
        "Unrated" => "Zonder leeftijdsbeperking.",
        "Children" => "Programma's aanbevolen voor kinderen. Het is een optionele classificatie, de omroepen zijn niet verplicht om dit aan te geven.",
        "NR" => "Geen beoordelingsinformatie."
    ],
    "RU" => [
        "16+" => "Alleen tieners van 16 jaar of ouder kunnen kijken.",
        "18+" => "Beperkt tot personen van 18 jaar of ouder.",
        "6+" => "Alleen kinderen van 6 jaar of ouder kunnen kijken.",
        "12+" => "Alleen kinderen van 12 jaar of ouder kunnen kijken.",
        "0+" => "Kan op elke leeftijd worden bekeken."
    ],
    "GB" => [
        "12" => "Films met classificatie 12A en videowerken met classificatie 12 bevatten materiaal dat over het algemeen niet geschikt is voor kinderen jonger dan 12 jaar.",
        "15" => "Niemand onder de 15 mag een film van 15 jaar in de bioscoop zien of een video met een rating van 15 kopen / huren. Werken met een rating van 15 zijn niet geschikt voor kinderen onder de 15 jaar.",
        "18" => "Films met classificatie 18 zijn voor volwassenen. Niemand onder de 18 mag een film van 18 jaar in de bioscoop zien of een video met classificatie 18 kopen / huren. Geen 18 werken met classificatie zijn geschikt voor kinderen.",
        "PG" => "PG staat voor Parental Guidance. Dit betekent dat een film geschikt is om algemeen bekeken te worden, maar sommige scènes zijn mogelijk niet geschikt voor jonge kinderen. Een PG-film mag een kind van rond de acht jaar of ouder niet van streek maken.",
        "12A" => "Films met classificatie 12A en videowerken met classificatie 12 bevatten materiaal dat over het algemeen niet geschikt is voor kinderen jonger dan 12 jaar. 12A vereist dat een volwassene een kind onder de 12 vergezelt bij het zien van een 12A-film in de bioscoop.",
        "R18" => "De categorie R18 is een speciale en wettelijk beperkte classificatie, voornamelijk voor expliciete werken van instemmende seks of sterk fetisjmateriaal waarbij volwassenen betrokken zijn.",
        "U" => "Het U-symbool staat voor Universal. Een U-film moet geschikt zijn voor kijkers van vier jaar en ouder."
    ],
    "SK" => [
        "7" => "Inhoud geschikt voor kinderen vanaf 6 jaar.",
        "12" => "Inhoud geschikt voor kinderen vanaf 12 jaar.",
        "15" => "Inhoud geschikt voor tieners ouder dan 15 jaar.",
        "18" => "Inhoud exclusief voor volwassenen.",
        "NR" => "Geen beoordelingsinformatie."
    ],
    "ES" => [
        "7" => "Niet aanbevolen voor kijkers onder de 7 jaar.",
        "10" => "Niet aanbevolen voor kijkers onder de 10 jaar.",
        "12" => "Niet aanbevolen voor kijkers onder de 12 jaar.",
        "13" => "Niet aanbevolen voor kijkers jonger dan 13 jaar.",
        "16" => "Niet aanbevolen voor kijkers jonger dan 16 jaar.",
        "18" => "Niet aanbevolen voor kijkers onder de 18 jaar.",
        "TP" => "Voor algemene weergave.",
        "NR" => "Geen beoordelingsinformatie.",
        "Infantil" => "Speciaal aanbevolen voor jongere kinderen."
    ],
    "LT" => [
        "NR" => "Geen beoordelingsinformatie.",
        "N-7" => "Bedoeld voor kijkers vanaf 7 jaar.",
        "N-14" => "Bedoeld voor kijkers vanaf 14 jaar en uitgezonden van 21 (21 uur) tot 6 (6 uur) uur.",
        "S" => "Bedoeld voor volwassen kijkers vanaf 18 jaar (overeenkomend met de leeftijdscategorie N-18) en uitgezonden tussen 23 (23 uur) en 6 (6 uur) uur; Beperkt tot minderjarigen en bedoeld voor volwassen kijkers. "
    ],
    'NL' => [
        "6" => "Mogelijk schadelijk tot 6.",
        "9" => "Mogelijk schadelijk tot 9.",
        "12" => "Mogelijk schadelijk tot 12.",
        "16" => "Mogelijk schadelijk tot 16.",
        "NR" => "Geen classificatie.",
        "AL" => "Alle leeftijden."
    ],
    "FR" => [
        "10" => "Niet aanbevolen voor kinderen onder de 10. Niet toegestaan ​​in kindertelevisieseries.",
        "12" => "Niet aanbevolen voor kinderen onder de 12 jaar. Lucht niet toegestaan ​​voor 10 uur => 00 uur. Sommige kanalen en programma's zijn onderworpen aan een uitzondering.",
        "16" => "Niet aanbevolen voor kinderen onder de 16 jaar. Lucht niet toegestaan ​​voor 10 uur => 30 uur. Sommige kanalen en programma's zijn onderhevig aan uitzonderingen.",
        "18" => "Niet aanbevolen voor personen onder de 18 jaar. Toegestaan ​​tussen middernacht en 5 uur 's ochtends en alleen op sommige kanalen is de toegang tot deze programma's vergrendeld met een persoonlijk wachtwoord.",
        "NR" => "Geen beoordelingsinformatie."
    ]
];
