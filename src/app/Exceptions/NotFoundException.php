<?php

namespace Nomercy\Themoviedb\Exceptions;

use Exception;

class NotFoundException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json([
            "status" => 'error',
            "status_code" => 404,
            'status_error' => __('themoviedb::error.404'),
            'status_message' => __('themoviedb::error.404'),
        ], 401);
    }
}
