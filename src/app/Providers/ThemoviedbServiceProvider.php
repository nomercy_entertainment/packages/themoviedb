<?php

namespace Nomercy\Themoviedb\Providers;

use Illuminate\Support\ServiceProvider;
use Nomercy\Themoviedb\Controllers\DataController;
use Nomercy\Themoviedb\Http\Controllers\Helper\Globals;
use Nomercy\Themoviedb\Http\Controllers\Helper\Loggers;
use Nomercy\Themoviedb\Controllers\ThemoviedbController;

class ThemoviedbServiceProvider extends ServiceProvider
{

    public function boot()
    {

        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'themoviedb');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'themoviedb');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');

        $this->publishes([
            __DIR__.'/../../config/config.php' => config_path('themoviedb'),
        ], 'config');

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/themoviedb'),
        ], 'views');

        $this->publishes([
            __DIR__.'/../../resources/assets' => public_path('vendor/themoviedb'),
        ], 'assets');

        $this->publishes([
            __DIR__.'/../../resources/lang' => resource_path('lang/vendor/themoviedb'),
        ], 'lang');

        if ($this->app->runningInConsole()) {
            $this->commands([

            ]);
        }

    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/config.php', 'themoviedb');

        $this->app->singleton('themoviedb', function () {
            return new ThemoviedbController;
        });

        $this->app->bind('helper', function(){
            return new Globals();
        });

        $this->app->bind('logger', function($arg){
            return new Loggers($arg);
        });

        $this->app->bind('data', function($arg){
            return new DataController($arg);
        });

        $this->app->bind('tmdb', function($arg){
            return new ThemoviedbController($arg);
        });
    }
}
