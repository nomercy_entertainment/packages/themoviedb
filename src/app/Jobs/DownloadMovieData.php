<?php


namespace Nomercy\Themoviedb\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Nomercy\Themoviedb\Facades\Helper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Nomercy\Themoviedb\Facades\Logger;
use Nomercy\Themoviedb\Http\Controllers\Database\DatabaseMovie;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientMovie;

class DownloadMovieData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = '/cache/build/movie/' . $this->id . '.json';
        $update = Helper::checkUpdate('movie', $this->id);
        $update = true;

        if ($update) {
            $movieClient = new ClientMovie();
            $movie = $movieClient->build($this->id);
            $data = collect($movie)->merge([]);

            Storage::put($file, json_encode($data));

            (new DatabaseMovie())->build($data);
            return $data;
        } else if (Storage::exists($file)) {

            $data = json_decode(utf8_decode(Storage::get($file)), true);

            (new DatabaseMovie())->build($data);

            DB::raw("DELETE FROM `failed_jobs` WHERE  `exception` like '%MaxAttemptsExceededException%'");

            // return $data;
        }
        else{
            Logger::info('movie: ' . $this->id . ' doesn\'t exist');
        }
    }

}
