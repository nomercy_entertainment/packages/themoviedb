<?php

namespace Nomercy\Themoviedb\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Nomercy\Themoviedb\Facades\Helper;
use Nomercy\Themoviedb\Facades\Logger;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;


use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Contracts\Queue\ShouldBeUnique;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientTv;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientSeason;
use Nomercy\Themoviedb\Http\Controllers\Database\DatabaseTv;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientEpisode;

class DownloadTVData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $file = '/cache/build/tv/' . $this->id . '.json';
        $update = Helper::checkUpdate('tv', $this->id);
        // $update = false;

        if ($update) {
            $tvClient = new ClientTv();
            $tv = $tvClient->build($this->id);

            if (isset($tv['seasons']) && count($tv['seasons']) > 0) {
                $data = collect($tv)->merge([
                    'seasons' => collect($tv['seasons'])->map(function ($season) {
                        $this->season = (new ClientSeason())->build($this->id, $season);
                        if (isset($this->season['episodes']) && count($this->season['episodes']) > 0) {
                            return collect($this->season)->merge([
                                'episodes' => collect($this->season['episodes'])->map(function ($episode) {
                                    $episode = (new ClientEpisode())->build($this->id, $episode['season_number'], $episode['episode_number'], $this->season['id']);
                                    return collect($episode);
                                }),
                            ]);
                        }
                    }),
                ]);

                Storage::put($file, json_encode($data));

                (new DatabaseTv())->build($data, $update);
                // return $data;
            } else {
                // return $tv;
            }
        } else if (Storage::exists($file)) {

            $data = json_decode(utf8_decode(Storage::get($file)), true);

            (new DatabaseTv())->build($data, true);
            // return $data;
            DB::raw("DELETE FROM `failed_jobs` WHERE  `exception` like '%MaxAttemptsExceededException%'");
        }
        else{
            Logger::info('movie: ' . $this->id . ' doesn\'t exist');
        }
    }
}
