<?php

namespace Nomercy\Themoviedb\Controllers;

use Carbon\Carbon;
use Nomercy\Themoviedb\Models\Tv;
use App\Http\Controllers\Controller;

use Nomercy\Themoviedb\Models\Genre;

use Nomercy\Themoviedb\Models\Movie;
use Illuminate\Support\Facades\Cache;
use Nomercy\Themoviedb\Models\Season;
use Nomercy\Themoviedb\Facades\Logger;
use Nomercy\Themoviedb\Models\Episode;
use Illuminate\Support\Facades\Storage;
use Nomercy\Themoviedb\Models\Language;
use Nomercy\Themoviedb\Models\Certification;
use Nomercy\Themoviedb\Models\Recommendation;
use Nomercy\Themoviedb\Exceptions\NotFoundException;


class DataController extends Controller
{

    public function tv($id)
    {
        $this->id = $id;

        $key = '_data_tv_' . $id . '_' . app()->getLocale();
        // return Cache::rememberForever($key, function () {
            $m =  Tv::where('id', $this->id)
            ->with('cast')
            ->with('crew')
            ->with('genres')
            ->with('posters')
            ->with('media')
            ->with('genres')
            ->with('videos')
            ->with('backdrops')
            ->with('lead_actors')
            ->with('recommendations')
            ->with('seasons.episodes')
            ->first();

                if ($m == null) {
                    Logger::notFound($this->id);
                }
                else{
                    return collect($m)->merge([
                        'languages' => 'en',
                        'lead_actors' => $m['lead_actors'] ?? [],
                        'subtitles' => $m['subtitles'] ?? [['']],
                        'origin' => env('APP_URL'),
                        'type' => 'tv',
                    ])->sortKeys();
                }
        // });

    }

    public function season($id, $season)
    {

        $this->id = $id;
        $this->season = $season;

        $key = '_data_season_' . $id . '_' . $season . '_' . app()->getLocale();
        // return Cache::rememberForever($key, function () {
            $m = Season::where('tv_id', $this->id)
                ->where('season_number', $this->season)
                ->with('episodes')
                ->get();

            if ($m->count() == 0) {
                Logger::notFound($this->id);
            };
            return $m;
        // });

    }

    public function episode($id, $season, $episode)
    {

        $this->id = $id;
        $this->season = $season;
        $this->episode = $episode;

        $key = '_data_episode_' . $id . '_' . $season . '_' . $episode . '_' . app()->getLocale();
        // return Cache::rememberForever($key, function () {
            $m = Episode::where('tv_id', $this->id)
            ->where('season_number', $this->season)
            ->where('episode_number', $this->episode)
            ->get();

            if ($m->count() == 0) {
                Logger::notFound($this->id);
            }
        // });

    }

    public function movie($id)
    {
        $this->id = $id;

        $key = '_data_movie_' . $id . '_' . app()->getLocale();
        // return Cache::rememberForever($key, function () {
            $m = Movie::where('id', $this->id)
                ->with('cast')
                ->with('crew')
                ->with('genres')
                ->with('posters')
                ->with('media')
                ->with('genres')
                ->with('videos')
                ->with('backdrops')
                ->with('recommendations')
                ->first();

            if ($m == null || $m->count() == 0) {
                Logger::notFound($this->id);
            }
            else{
                return collect($m)->merge([
                    'languages' => 'en',
                    'type' => 'movie',
                    'lead_actors' => $m['lead_actors'] ?? [],
                    'subtitles' => $m['subtitles'] ?? [['']],
                    'origin' => env('APP_URL'),

                ])->sortKeys();
            }
        // });

    }

    public function genres()
    {
        $key = '_data_genres.' . app()->getLocale();
        // return Cache::rememberForever($key, function () {
            return Genre::where('id', $this->id)
                ->get();
        // });

    }

    public static function ratings()
    {
        $key = '_data_ratings.' . app()->getLocale();
        // return Cache::rememberForever($key, function () {
            return Certification::where('iso_3166_1', session('country') ?? app()->getLocale())
            ->get();
        // });

    }

    public static function certifications()
    {

        $key = '_data_certifications.' . app()->getLocale();
        // return Cache::rememberForever($key, function () {
            $l = [];
            foreach (Certification::get() as $item) {
                $l[$item['iso_3166_1']][$item['rating']] = $item['meaning'];
            }
            return $l;
        // });

    }

    public function tv_recommendations($id)
    {
        $this->id = $id;

        $key = '_data_tv_recommendations_' . $id . '_' . app()->getLocale();
        // return Cache::rememberForever($key, function () {
            return TV::where('id', $this->id)
                ->with('recommendations')
                ->first()['recommendations'] ?? [];
        // });

    }

    public function movie_recommendations($id)
    {
        $this->id = $id;

        $key = '_data_movie_recommendations_' . $id . '_' . app()->getLocale();
        // return Cache::rememberForever($key, function () {
            return Movie::where('id', $this->id)
                ->with('recommendations')
                ->first()['recommendations'] ?? [];
        // });
    }

    public function renew_cache()
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);

        $carbon = collect(Carbon::getAvailableLocales())->map(function($lang) {
            return strtolower(collect(explode('_', $lang))->first());
        });

        $langs = Language::whereIn('iso_639_1', $carbon)->pluck('iso_639_1');


        foreach($langs as $lang){
            app()->setLocale($lang);
            Logger::info('caching ratings in language: ' . $lang);
            $this->ratings();
        }
        foreach($langs as $lang){
            app()->setLocale($lang);
            Logger::info('caching certifications in language: ' . $lang);
            $this->certifications();
        }

        foreach(Tv::select('id')->get()->pluck('id') as $id){
            foreach($langs as $lang){
                app()->setLocale($lang);
                Logger::info('caching tv: ' . $id . ' language: ' . $lang);
                $this->tv($id);
                $this->recommendations($id);
            }
        }

        foreach(Movie::select('id')->get()->pluck('id') as $id){
            foreach($langs as $lang){
                app()->setLocale($lang);
                Logger::info('caching movie: ' . $id . ' language: ' . $lang);
                $this->movie($id);
                $this->recommendations($id);
            }
        }
    }
}
