<?php

namespace Nomercy\Themoviedb\Http\Controllers\Helper;

use Carbon\Carbon;
use Archive7z\Archive7z;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use Nomercy\Themoviedb\Models\Cache;
use Nomercy\Themoviedb\Facades\Logger;
use Illuminate\Support\Facades\Storage;

class Globals extends Controller
{

    /**
     * Get the OS Laravel is running on.
     *
     * @return void
     */
    function os(){
        switch (PHP_OS) {
            case 'CYGWIN_NT-5.1':
                return 'windows';
                break;

            case 'Darwin':
                    return 'macos';
                break;

            case 'FreeBSD':
                return 'bsd';
                break;

            case 'HP-UX':
                return 'ux';
                break;

            case 'IRIX64':
                return 'irix';
                break;

            case 'Linux':
                return 'linux';
                break;

            case 'NetBSD':
                return 'bsd';
                break;

            case 'WIN32':
                return 'windows';
                break;

            case 'WINNT':
                return 'windows';
                break;

            case 'Windows':
                return 'windows';
                break;

            default:
                return strtoupper(substr(PHP_OS, 0, 3));
                break;
        }
    }

    /**
     * Download FFmpeg binaries for Linux.
     *
     * @return void
     */
    function download_ffmpeg_for_linux(){

        $ffmpeg = 'ffmpeg/linux/ffmpeg';
        $ffprobe = 'ffmpeg/linux/ffprobe';

        if(!Storage::exists($ffmpeg) || !Storage::exists($ffprobe) ){

            $url = 'https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-amd64-static.tar.xz';

            $link = explode('/', $url);
            $fileName = $link[count($link) -1];
            $file = storage_path('app/tmp/'. $fileName);
            $folder = storage_path('app/ffmpeg/linux/');
            system('mkdir -p ' . $folder);

            if(!Storage::exists($file)){
                copy($url, $file);
            }

            $tar_exec = "tar -x -C " . $folder . " --strip-components 1 -f " . $file . " --wildcards '*/ffmpeg' --wildcards '*/ffprobe'";
            shell_exec($tar_exec);
            system("rm -rf ".escapeshellarg($file));
        }
    }

    /**
     * Download FFmpeg binaries for Mac OS.
     *
     * @return void
     */
    function download_ffmpeg_for_macos(){

        $ffmpeg = 'ffmpeg/macos/ffmpeg';
        $ffprobe = 'ffmpeg/macos/ffprobe';

        if(!Storage::exists($ffmpeg)){

            $url = 'https://evermeet.cx/ffmpeg/getrelease/ffmpeg/zip';

            $link = explode('/', $url);
            $fileName = $link[count($link) -1];
            $file = storage_path('app/tmp/'. $fileName);
            $folder = storage_path('app/ffmpeg/macos/');
            system('mkdir -p ' . $folder);

            if(!Storage::exists($file)){
                copy($url, $file);
            }

            $unzip_exec = "unzip -o -d " . $folder . " -j " . $file . " ffmpeg";
            shell_exec($unzip_exec);
            system("rm -rf ".escapeshellarg($file));
        }

        if(!Storage::exists($ffprobe) ){

            $url = 'https://evermeet.cx/ffmpeg/getrelease/ffprobe/zip';

            $link = explode('/', $url);
            $fileName = $link[count($link) -1];
            $file = storage_path('app/tmp/'. $fileName);
            $folder = storage_path('app/ffmpeg/macos/');
            system('mkdir -p ' . $folder);

            if(!Storage::exists($file)){
                copy($url, $file);
            }

            $unzip_exec = "unzip -o -d " . $folder . " -j " . $file . " ffprobe";
            shell_exec($unzip_exec);
            system("rm -rf ".escapeshellarg($file));
        }
    }

    /**
     * Download FFmpeg binaries for Windows.
     *
     * @return void
     */
    function download_ffmpeg_for_windows(){

        $ffmpeg = '/ffmpeg/windows/ffmpeg.exe';
        $ffprobe = '/ffmpeg/windows/ffprobe.exe';

        if(!Storage::exists($ffmpeg) || !Storage::exists($ffprobe) ){

            $url = 'https://www.gyan.dev/ffmpeg/builds/ffmpeg-release-full.7z';

            $link = explode('/', $url);
            $fileName = $link[count($link) -1];
            $file = storage_path('app/tmp/'. $fileName);
            $folder = storage_path('app/ffmpeg/windows/');
            system('mkdir -p ' . $folder);

            if(!Storage::exists($file)){
                copy($url, $file);
            }

            $obj = new Archive7z($file);
            if (!$obj->isValid()) {
                throw new \RuntimeException('Incorrect archive');
            }
            $del = '';
            foreach ($obj->getEntries() as $entry) {
                if ($this->contains($path = $entry->getPath(), array('ffmpeg.exe', 'ffprobe.exe') )) {
                    $orig = $folder . $path;
                    $fileName = explode('/', $orig)[count(explode('/', $orig)) -1];
                    $fileName2 = explode('/', $orig)[count(explode('/', $orig)) -2];
                    $new = $folder . $fileName;

                    if(!file_exists($new)){
                        $entry->extractTo($folder);
                        rename($orig, $new);
                    }

                    $del = $folder . str_replace([$folder, $fileName2 . '/' . $fileName], '', $orig);
                }
            }
            system("rm -rf ".escapeshellarg($del));
            system("rm -rf ".escapeshellarg($file));
        }
    }

    /**
     * Check if binaries for OS are available.
     *
     * @return string
     */
    function check_binaries(){
        $os = $this->os();

        if ($os === 'linux' ){
            if(!file_exists(storage_path('app/ffmpeg/linux/ffmpeg')) || !file_exists(storage_path('app/ffmpeg/linux/ffprobe'))) {
                $this->download_ffmpeg_for_linux();
            }
            if( !file_exists(storage_path('app/youtube-dl/linux/youtube-dl'))) {
                system('mkdir -p ' . storage_path('app/youtube-dl/linux/'));
                copy('https://yt-dl.org/downloads/latest/youtube-dl', storage_path('app/youtube-dl/linux/youtube-dl'));
                shell_exec('chmod +x ' . storage_path('app/youtube-dl/linux/youtube-dl'));
            }
            shell_exec('mkdir -p /var/www/html/storage/app/assets/trailer');
            return 'linux';
        }

        else if ($os === 'windows' ){
            if(!file_exists(storage_path('app/ffmpeg/windows/ffmpeg.exe')) || !file_exists(storage_path('app/ffmpeg/windows/ffprobe.exe'))) {
                $this->download_ffmpeg_for_windows();
            }
            if(!file_exists(storage_path('app/youtube-dl/windows/youtube-dl.exe'))) {
                system('mkdir -p ' . storage_path('app/youtube-dl/windows/'));
                copy('https://yt-dl.org/downloads/latest/youtube-dl.exe', storage_path('app/youtube-dl/windows/youtube-dl.exe'));
            }
            return 'windows';
        }

        else if ($os === 'macos' ){
            if(!file_exists(storage_path('app/ffmpeg/macos/ffmpeg')) || !file_exists(storage_path('app/ffmpeg/macos/ffprobe'))) {
                $this->download_ffmpeg_for_macos();
            }
            if( !file_exists(storage_path('app/youtube-dl/linux/youtube-dl'))) {
                system('mkdir -p ' . storage_path('app/youtube-dl/linux/'));
                copy('https://yt-dl.org/downloads/latest/youtube-dl', storage_path('app/youtube-dl/linux/youtube-dl'));
                shell_exec('chmod +x ' . storage_path('app/youtube-dl/linux/youtube-dl'));
            }
            return 'macos';
        }

    }

    /**
     * Youtube-dl binary.
     *
     * @return path
     */
    function youtubeDl(){
        if ($this->check_binaries() != 'windows'){
            return storage_path('app/youtube-dl/linux/youtube-dl');
        }
        else {
            return storage_path('app/youtube-dl/windows/youtube-dl.exe');
        }
    }

    /**
     * Ffmpeg binary.
     *
     * @return path
     */
    function ffmpeg(){
        $os = $this->check_binaries();

        if ($os === 'linux' ){
            return storage_path('app/ffmpeg/linux/ffmpeg');
        }
        else if ($os === 'windows' ){
            return storage_path('app/ffmpeg/windows/ffmpeg.exe');
        }
        else {
            return storage_path('app/ffmpeg/macos/ffmpeg');
        }
    }

    /**
     * Ffprobe binary.
     *
     * @return path
     */
    function ffprobe(){
        $os = $this->check_binaries();

        if ($os === 'linux' ){
            return storage_path('app/ffmpeg/linux/ffprobe');
        }
        else if ($os === 'windows' ){
            return storage_path('app/ffmpeg/windows/ffprobe.exe');
        }
        else {
            return storage_path('app/ffmpeg/macos/ffprobe');
        }
    }

    /**
     * Check if a string contains any of list.
     *
     * @param string $haystack
     * @param array $needle
     * @param int $offset
     * @return bool
     */
    function contains(string $haystack, $needle, int $offset = 0)
    {
        if (!is_array($needle)) $needle = array($needle);
        foreach ($needle as $query) {
            if (strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
        }
        return false;
    }

    /**
     * Converts gender number to name.
     *
     * @param string $gender
     * @return string
     */
    function gender(string $gender){
        if($gender == 0){
            return 'Not specified';
        }
        else if($gender == 1){
            return 'Female';
        }
        else if($gender == 2){
            return 'Male';
        }
    }


    /**
     * Checks if updates are nessecary for a given movie or show.
     *
     * @param string $type
     * @param int $id
     * @return bool
     */
    function checkUpdate(string $type, int $id){

        Logger::info('Check ' . $type . ' ' . $id . ' for updating');

        $token = env('TMDB_APIKEY');
        $now = Carbon::now();
        $file = '/cache/build/' . $type . '/' . $id . '.json';

        $exists = Storage::exists($file);
        $diff = $exists ? Carbon::createFromTimestamp(Storage::lastModified($file))->diffInDays($now) : 0;

        if($diff > 0 || !$exists){

            $append = [
                'start_date=' . Carbon::now()
                    ->subWeeks(2)
                    ->format('md'),

                '&end_date=' . Carbon::now()
                    ->format('md'),
            ];

            $changes = Http::withToken($token)
                ->get('https://api.themoviedb.org/3/' . $type . '/' . $id . '/changes?' . implode('', $append))
                ->json();

            $time = Carbon::now()->subYear(1);
            $updated = collect(collect($changes)->first())->count() > 0;

            if($updated){
                Logger::info('Deleting ' . $type . ' ' . $id . ' from cache');

                Cache::where('key', 'LIKE', '%_client_' . $type . '_' . $id)->delete();
                Cache::where('key', 'LIKE', '%_data_%_' . $id . '_%')->delete();

                Logger::info('Updating ' . $type . ' ' . $id . '');
                foreach($changes as $change){
                    foreach($change as $item){
                        foreach($item['items'] as $update){
                            if(Carbon::createFromDate($update['time'])->gt($time)){
                                $time = $update['time'];
                            }
                        }
                    }
                }
            }
            $time = Carbon::createFromTimestampUTC($time)
                ->toDateString();


            $modified = $exists
            ? Carbon::createFromTimestampUTC(Storage::lastModified($file))->toDateString()
            : false;

            return Carbon::createFromDate($time)->gt($modified)
                || !Storage::exists($file);

        }
        else {
            Logger::info('not updating  ' . $type . ' ' . $id . '');
            return false;
        }
    }

    /**
     * takes the response from the moviedb and returns the image download list.
     *
     * @param array|object $images
     * @param string $type
     * @return array
     */
    function collectImages(array $images, string $type){
        $dl_images = [];

        if(isset($images['backdrops'])){
            foreach($images['backdrops'] as $key => $backdrop){
                $dl_images[] = [$backdrop['file_path'], $type, 'original'];
            }
        }
        if(isset($images['posters'])){
            foreach($images['posters'] as $key => $poster){
                $dl_images[] = [$poster['file_path'], $type, 'original'];
                $dl_images[] = [$poster['file_path'], $type, 'w300'];
            }
        }
        else if (isset($images)){
            foreach($images as $key => $image){
                if($this->contains($key, 'backdrop') == true){
                    $dl_images[] = [$image, $type, 'original'];
                }

                elseif($this->contains($key, 'poster') == true){
                    $dl_images[] = [$image, $type, 'original'];
                    $dl_images[] = [$image, $type, 'w300'];
                }
            }
        }

        return $dl_images;
    }

    /**
     * Get the full image url from themoviedb.
     *
     * @param string #img
     * @param string #size (original, w300)
     * @return url
     */
    function img_url($img, string $size = 'original')
    {
        if($img == null) return false;
        return 'https://image.tmdb.org/t/p/' . $size . $img;
    }

    /**
     * download array of images from themoviedb.
     *
     * @param array $arr
     * @param string $type
     * @return void
     */
    function download($arr, $type){
        foreach ($arr as $image) {
            $this->downloadImage($image[0], $type, $image[1]);
        }
    }

    /**
     * Download an images from themoviedb and convert to smaller size of size is original.
     *
     * @param string $path
     * @param string $type
     * @param string $size = original
     * @return void
     */
    function downloadImage(string $path, string $type, string $size = 'original')
    {
        if (!empty($path) && !empty($type)) {

            if ($size == 'original') {
                $location = str_replace('.jpg', '.original.jpg', $path);
            } else {
                $location = $path;
                $location2 = str_replace('.jpg', '.webp', $path);
            }

            if(!Storage::exists('assets/' . $type . $location) || isset($location2)){
                Storage::put('assets/' . $type . $location, @file_get_contents($this->img_url($path, $size), 0));
                if (isset($location2)) {
                    $this->convert_image(storage_path('app/assets/' . $type . $location), storage_path('app/assets/' . $type . $location2), 'y');
                }
            }
        }
    }

    /**
     * Convert image.
     *
     * @param string $file_in
     * @param string $file_out
     * @param bool $replace
     * @return void
     */
    function convert_image($file_in, $file_out, $replace)
    {
        if (file_exists($file_in)) {
            exec($this->ffmpeg()." -v quiet -stats -i " . $file_in . "  " . $file_out . " -" . $replace . " 2> /dev/null");
        }
        else{
            Logger::info('Can\'t convert image: ' . $file_in . ' because it does\'t exist');
        }
    }


    /**
     * Converts seconds to translated human hours and minutes.
     *
     * @param int $time
     * @return string "1 hour 35 minutes"
     */
    function hoursandmins(int $time)
    {
        if ($time < 1) {
            return;
        }
        $hours = (int) floor($time / 60);
        $minutes = ($time % 60);

        if ($hours > 0 && $minutes != 0) {
            $format = '%01d ' . trans_choice('themoviedb::time.hour', 1) . ', %02d ' . trans_choice('themoviedb::time.minute', $minutes);
            return sprintf($format, $hours, $minutes);
        }
        else if ($hours > 0 && $minutes == 0) {
            $format = '%01d ' . trans_choice('themoviedb::time.hour', 1);
            return sprintf($format, $hours);
        }
        else {
            $format = '%01d ' . trans_choice('themoviedb::time.minute', $minutes);
            return sprintf($format, $minutes);
        }
    }

    /**
     * Localized current time.
     *
     * @return Carbon instance
     */
    function now()
    {
        return Carbon::now()->format(__('themoviedb::time.format.dateTime'));
    }


    /**
     * Full public image path.
     *
     * @param string $path
     * @return string url
     */
    public function image_src($path)
    {
        return $path != null && config('themoviedb.download_images')
            ? asset('/assets/tv' . str_replace('.jpg', '.original.jpg', $path))
            : $this->img_url($path, 'original');
    }

    /**
     * Get the translated item.
     *
     * @param Model $model
     * @param string $item
     * @return string
     */
    public function translatable(Model $model, $item)
    {
        $trans = $model->translation()->pluck($item)->first();
        return $trans != null && $trans != ''
            ? $trans
            : $model->$item;
    }

}
