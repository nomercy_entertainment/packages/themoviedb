<?php

namespace Nomercy\Themoviedb\Http\Controllers\Helper;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Nomercy\Themoviedb\Facades\Helper;
use Nomercy\Themoviedb\Exceptions\NotFoundException;

class Loggers extends Controller
{
    public $arg;

    /**
     * Dump with localized timestamp.
     *
     * @param string $arg
     * @return void Console output
     */
    public function dump($arg)
    {
        if (app()->runningInConsole()) {
            dump(Helper::now() . ': ' . $arg);
        }
    }
    /**
     * Green echo with localized timestamp.
     *
     * @param string $arg
     * @return void Console output
     */
    public function info($arg)
    {
        if (app()->runningInConsole()) {
            echo Helper::now() . ":\033[32m " . $arg . "\033[0m\n";
        }
    }
    /**
     * Yellow echo with localized timestamp.
     *
     * @param string $arg
     * @return void Console output
     */
    public function warning($arg)
    {
        if (app()->runningInConsole()) {
            echo Helper::now() . ":\033[33m " . $arg . "\033[0m\n";
        }
    }
    /**
     * Red echo with localized timestamp.
     *
     * @param string $arg
     * @return void Console output
     */
    public function error($arg)
    {
        if (app()->runningInConsole()) {
            echo Helper::now() . ":\033[31m " . $arg . "\033[0m\n";
        }
    }
    /**
     * Red echo with localized timestamp and trows NotFoundException.
     *
     * @param string $arg
     * @return void Console output
     * @return exception
     */
    public function notFound($arg)
    {
        if(app()->runningInConsole()){
            echo Helper::now() . ":\033[31m " . $arg . "\033[0m\n";
            return 0;
        }
        throw new NotFoundException();
    }



}
