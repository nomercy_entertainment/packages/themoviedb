<?php

namespace Nomercy\Themoviedb\Http\Controllers\Client;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Nomercy\Themoviedb\Facades\Helper;
use Nomercy\Themoviedb\Facades\Logger;
use Illuminate\Support\Facades\Storage;

class ClientTv extends Controller
{
    public $token, $id, $lang, $tv;

    public function __construct($lang = null)
    {
        $this->token = env('TMDB_APIKEY');
    }

    /**
     * Fetches Tv response.
     *
     * @param  integer  $id
     * @return array
     */
    public function build($id){

        $this->id = $id;

        $key = '_client_tv_' . $id;
        // return Cache::rememberForever($key, function () {

            $file = '/cache/client/tv/' . $this->id . '/' . $this->id . '.json';
            if(Storage::exists($file)){
                return json_decode(utf8_decode(Storage::get($file)), true);
            }

            $append = [
                'alternative_titles',
                'content_ratings',
                'credits',
                'external_ids',
                'images',
                'keywords',
                'recommendations',
                'reviews',
                'similar',
                'translations',
                'videos',
                'watch/providers',
            ];

            $this->translations = [];

            $this->tv = Http::withToken($this->token)->withoutRedirecting()
                ->get('https://api.themoviedb.org/3/tv/' . $this->id . '?append_to_response=' . implode(',', $append))
                ->json();

            if(!isset($this->tv['id'])){
                Logger::warning('Adding tv ' . $this->id . ' Does\'t exist.');
                return 0;
            }

            $image = collect($this->tv['images'])->map(function($posters) {
                $data = collect($posters)->where('iso_639_1', '==', 'en')->pluck('file_path')->first();
                if($data == null){
                    $data = collect($posters)->pluck('file_path')->first();
                }
                return $data;
            });

            $trailer = collect($this->tv['videos'])->map(function($trailer) {
                return collect($trailer)->where('type', '==', 'Trailer')->first();
            })->pluck('key')->first();

            $videos = collect($this->tv['videos']['results'])->map(function($video) {
                return collect($video)->merge([
                    'tv_id' => $this->id,
                    'type' => $video['type'],
                ])->except('id');
            });

            $list = collect($this->tv)->merge([
                'alternative_titles' => collect(isset($this->tv['alternative_titles']) ? $this->tv['alternative_titles']['results'] : [])->map(function($at){
                return collect($at)->merge([
                    'tv_id' => $this->id,
                    $at
                    ])->except(0, 'type');

                }),
                'backdrop' => $image['backdrops'],
                'poster' => $image['posters'],
                'backdrop_path' => $this->tv['backdrop_path'],
                'content_ratings' => collect(isset($this->tv['content_ratings']) ? $this->tv['content_ratings']['results'] : [])->map(function($cr){
                    return collect($cr)->merge([
                        // 'tv_id' => $this->id,
                        $cr
                    ])->except(0);
                }),
                'created_by' => collect($this->tv['created_by'])->map(function($credits){
                    return collect($credits)->merge([
                        'profile_id' => $credits['id'],
                        'known_for_department' => isset($credits['known_for_department']) ? $credits['known_for_department'] : null,
                    ])->except('id');
                }),
                'cast' => collect($this->tv['credits']['cast'])->map(function($credits){
                    return collect($credits)->merge([
                        'profile_id' => $credits['id'],
                        'known_for_department' => isset($credits['known_for_department']) ? $credits['known_for_department'] : null,
                    ])->except('id');
                }),
                'crew' => collect($this->tv['credits']['crew'])->map(function($credits){
                    return collect($credits)->merge([
                        'profile_id' => $credits['id'],
                        'known_for_department' => isset($credits['known_for_department']) ? $credits['known_for_department'] : null,
                    ])->except('id');
                }),
                'duration' => collect($this->tv['episode_run_time'])->first(),
                "external_ids" => collect($this->tv['external_ids'])->only('tvdb_id','imdb_id'),
                "external_links" => [
                    "imdb" => 'https://www.imdb.com/title/'. $this->tv['external_ids']['imdb_id'],
                    "tvdb" => 'https://thetvdb.com/?tab=series&id='. $this->tv['external_ids']['tvdb_id'],
                    "facebook" => 'https://facebook.com/'. $this->tv['external_ids']['facebook_id'],
                    "instagram" => 'https://instagram.com/'. $this->tv['external_ids']['instagram_id'],
                    "twitter" => 'https://twitter.com/'. $this->tv['external_ids']['twitter_id'],
                    'tv_id' => $this->id,
                ],
                'first_air_date' => Carbon::createFromDate($this->tv['first_air_date'])->format('Y-m-d'),
                'genres' => collect($this->tv['genres'])->pluck('id'),
                'homepage' => $this->tv['homepage'],
                'id' => collect($this->tv)->only('id')->first(),
                'images' => [
                    "backdrops" => collect($this->tv['images']['backdrops'])->map(function($backdrops) {
                        return collect($backdrops)->merge([
                            'tv_id' => $this->id
                        ]);
                    }),
                    "posters" => collect($this->tv['images']['posters'])->map(function($posters) {
                        return collect($posters)->merge([
                            'tv_id' => $this->id
                        ]);
                    }),
                ],
                'in_production' => $this->tv['in_production'],
                'keywords' => collect($this->tv['keywords']['results'] ?? null)->pluck('name'),
                'languages' => $this->tv['languages'],
                'last_air_date' => Carbon::createFromDate($this->tv['last_air_date'])->format('Y-m-d'),
                'last_episode_to_air' => collect($this->tv['last_episode_to_air'])->only('id')->first(),
                'title' => $this->tv['name'],
                'title_sort' => preg_replace("/[^A-Za-z0-9 ]/", '', str_replace(['The ', 'A ', 'An ', "'", '"'], ['','','','',''], $this->tv['name'])),
                'networks' => collect($this->tv['networks'])->map(function($nw){
                    return collect(Http::withToken($this->token)
                    ->get('https://api.themoviedb.org/3/network/' . $nw['id'] . '?append_to_response=translations')
                    ->json())->merge([
                        // 'tv_id' => $this->id
                    ]);
                }),
                'next_episode_to_air' => collect($this->tv['next_episode_to_air'])->only('id')->first(),
                'number_of_episodes' => $this->tv['number_of_episodes'],
                'number_of_seasons' => $this->tv['number_of_seasons'],
                'origin_country' => collect($this->tv['origin_country'])->first(),
                'original_language' => collect($this->tv['original_language'])->first(),
                'overview' => $this->tv['overview'],
                'popularity' => $this->tv['popularity'],
                'poster_path' => $this->tv['poster_path'],
                'production_companies' => collect($this->tv['production_companies'])->map(function($pc){
                    return collect(Http::withToken($this->token)
                    ->get('https://api.themoviedb.org/3/company/' . $pc['id'] . '?append_to_response=translations')
                    ->json())->except(['description','parent_company'])->merge([
                        // 'tv_id' => $this->id
                    ]);
                }),
                'production_countries' => $this->tv['production_countries'],
                'recommendations' => collect(isset($this->tv['recommendations']) ? $this->tv['recommendations']['results'] : [])->map(function($recommendations) {
                    return collect($recommendations)->only('id', 'name', 'overview', 'poster_path', 'vote_average')->merge([
                        'recommendation_tv_id' => $recommendations['id'],
                        'tv_id' => $this->id,
                        'title' => $recommendations['name'],
                        'title_sort' => preg_replace("/[^A-Za-z0-9 ]/", '', str_replace(['The ', 'A ', 'An ', "'", '"'], ['','','','',''], $recommendations['name'])),
                        'backdrop' => $recommendations['backdrop_path'],
                        'poster' => $recommendations['poster_path'],
                    ])->except('poster_path','backdrop_path', 'id', 'name');
                }),
                'reviews' => collect($this->tv['reviews']['results']),
                'seasons' => collect($this->tv['seasons'])->pluck(['season_number']),
                'similars' => collect($this->tv['similar']['results'])->map(function($similar) {
                    return collect($similar)->merge([
                        'similar_tv_id' => $similar['id'],
                        'tv_id' => $this->id,
                        'title' => $similar['name'],
                        'title_sort' => preg_replace("/[^A-Za-z0-9 ]/", '', str_replace(['The ', 'A ', 'An ', "'", '"'], ['','','','',''], $similar['name'])),
                        'backdrop' => $similar['backdrop_path'],
                        'poster' => $similar['poster_path'],
                    ])->only('similar_tv_id','tv_id', 'title', 'title_sort', 'backdrop', 'poster', 'overview');
                }),
                'spoken_languages' => collect($this->tv['spoken_languages'])->pluck('iso_639_1'),
                'status' => $this->tv['status'],
                'translations' => collect(isset($this->tv['translations']) ? $this->tv['translations']['translations'] : [])->map(function($translations) {
                    return collect($translations)->merge([
                        'iso_639_1' => $translations['iso_639_1'],
                        'iso_3166_1' => $translations['iso_3166_1'],
                        'name' => $translations['name'],
                        'english_name' => $translations['english_name'],
                        'title' => $translations['data']['name'],
                        'overview' => $translations['data']['overview'],
                        'tv_id' => $this->tv['id']
                    ])->except('data');
                })->filter(function($i) {
                    return $i != null ? $i : null;
                }),
                'person_translations' => collect($this->translations)->filter(function($i) {
                    return $i != null ? $i : null;
                }),
                'videos' => $videos,
                'trailer'  => $trailer,
                'vote_average' => $this->tv['vote_average'],
                'vote_count' => $this->tv['vote_count'],
                'watch/providers' => collect($this->tv['watch/providers']['results']),

            ])->except([
                'production_countries',
                'languages',
                'spoken_languages',
                'watch/providers',
                'episode_run_time',
                'vote_count',
                'credits',
                'original_name',
                'tagline',
                'type',
                'name',
            ])->toArray();

            ksort($list);
            // dd($list);

            Storage::put($file, json_encode($list));

            return $list;
        // });
    }
}
