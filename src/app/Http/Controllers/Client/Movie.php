<?php

namespace Nomercy\Themoviedb\Http\Controllers\Client;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Nomercy\Themoviedb\Facades\Logger;
use Illuminate\Support\Facades\Storage;

class ClientMovie extends Controller
{
    public $token, $id;

    public function __construct()
    {
        $this->token = env('TMDB_APIKEY');
    }

    /**
     * Fetches Movie response.
     *
     * @param  integer  $id
     * @return array
     */
    public function build($id){

        $this->id = $id;
        $key = '_client_movie_' . $id;
        // return Cache::rememberForever($key, function () {

            $file = '/cache/client/movie/' . $this->id . '.json';
            if(Storage::exists($file)){
                return json_decode(utf8_decode(Storage::get($file)), true);
            }

            $append = [
                'alternative_titles',
                'content_ratings',
                'credits',
                'external_ids',
                'images',
                'keywords',
                'recommendations',
                'release_dates',
                'reviews',
                'similar',
                'translations',
                'videos',
                'watch/providers',
            ];

            $this->translations = [];

            $this->movie = Http::withToken($this->token)->withoutRedirecting()
                ->get('https://api.themoviedb.org/3/movie/' . $this->id . '?append_to_response=' . implode(',', $append))
                ->json();

            if(!isset($this->movie['id'])){
                Logger::warning('Adding movie ' . $this->id . ' Does\'t exist.');
                return 0;
            }

            $image = collect($this->movie['images'] ?? [])->map(function($posters) {
                $data = collect($posters)->where('iso_639_1', '==', 'en')->pluck('file_path')->first();
                if($data == null){
                    $data = collect($posters)->pluck('file_path')->first();
                }
                return $data;
            });

            $trailer = collect($this->movie['videos'] ?? [])->map(function($trailer) {
                return collect($trailer)->where('type', '==', 'Trailer')->first();
            })->pluck('key')->first();

            $videos = collect($this->movie['videos']['results'] ?? [])->map(function($video) {
                return collect($video)->merge([
                    'movie_id' => $this->id,
                    'type' => $video['type'],
                ])->except('id');
            });

            $alternate = isset($this->movie['alternative_titles']) ? $this->movie['alternative_titles']['titles'] : [];

            $list = collect($this->movie)->merge([
                'alternative_titles' => collect($alternate)->map(function($at){
                return collect($at)->merge([
                    'movie_id' => $this->id,
                    $at
                    ])->except(0, 'type');
                }),
                'backdrop' => $image['backdrops'],
                'poster' => $image['posters'],
                'backdrop_path' => $this->movie['backdrop_path'],
                'reviews' => collect(isset($this->movie['reviews']) ? $this->movie['reviews']['results'] : [])->map(function($reviews){
                    $review = Http::withToken($this->token)
                    ->get('https://api.themoviedb.org/3/review/' . $reviews['id'] . '?append_to_response=translations')
                    ->json();

                    return collect($review)->merge([
                        "movie_id" => $review['media_id'],
                        "author_name" => $review['author_details']['name'],
                        "author_username" => $review['author_details']['username'],
                        "author_avatar_path" => $review['author_details']['avatar_path'],
                        "author_rating" => $review['author_details']['rating'],
                    ])->except('media_id', 'media_title','media_type', 'author_details');

                }),

                'cast' => collect($this->movie['credits']['cast'])->map(function($credits){
                    return collect($credits)->merge([
                        'profile_id' => $credits['id'],
                        'known_for_department' => isset($credits['known_for_department']) ? $credits['known_for_department'] : null,
                    ])->except('id');
                }),
                'crew' => collect($this->movie['credits']['crew'])->map(function($credits){
                    return collect($credits)->merge([
                        'profile_id' => $credits['id'],
                        'known_for_department' => isset($credits['known_for_department']) ? $credits['known_for_department'] : null,
                    ])->except('id');
                }),
                'duration' => collect($this->movie['runtime'])->first(),
                "external_ids" => collect($this->movie['external_ids'] ?? [])->only('tvdb_id','imdb_id'),
                "external_links" => [
                    "imdb" => isset($this->movie['external_ids']) ? 'https://www.imdb.com/title/'. $this->movie['external_ids']['imdb_id'] : null,
                    "tvdb" => isset($this->movie['external_ids']['tvdb_id']) ? 'https://thetvdb.com/?tab=series&id='. $this->movie['external_ids']['tvdb_id'] : null,
                    "facebook" => isset($this->movie['external_ids']) ? 'https://facebook.com/'. $this->movie['external_ids']['facebook_id'] : null,
                    "instagram" => isset($this->movie['external_ids']) ? 'https://instagram.com/'. $this->movie['external_ids']['instagram_id'] : null,
                    "twitter" => isset($this->movie['external_ids']) ? 'https://twitter.com/'. $this->movie['external_ids']['twitter_id'] : null,
                    'movie_id' => $this->id,
                ],
                'release_date' => Carbon::createFromDate($this->movie['release_date'])->format('Y-m-d'),
                'genres' => collect($this->movie['genres'])->pluck('id'),
                'homepage' => $this->movie['homepage'],
                'id' => collect($this->movie)->only('id')->first(),
                'images' => [
                    "backdrops" => collect($this->movie['images']['backdrops'])->map(function($backdrops) {
                        return collect($backdrops)->merge([
                            'movie_id' => $this->id
                        ]);
                    }),
                    "posters" => collect($this->movie['images']['posters'])->map(function($posters) {
                        return collect($posters)->merge([
                            'movie_id' => $this->id
                        ]);
                    }),
                ],
                'keywords' => collect($this->movie['keywords']['results'] ?? null)->pluck('name'),
                'title' => $this->movie['title'],
                'title_sort' => preg_replace("/[^A-Za-z0-9 ]/", '', str_replace(['The ', 'A ', 'An ', "'", '"'], ['','','','',''], $this->movie['title'])),
                'original_language' => collect($this->movie['original_language'])->first(),
                'overview' => $this->movie['overview'],
                'popularity' => $this->movie['popularity'],
                'poster_path' => $this->movie['poster_path'],
                'production_companies' => collect($this->movie['production_companies'])->map(function($pc){
                    return collect(Http::withToken($this->token)
                    ->get('https://api.themoviedb.org/3/company/' . $pc['id'] . '?append_to_response=translations')
                    ->json())->except(['description','parent_company'])->merge([
                        'movie_id' => $this->id
                    ]);
                }),
                'production_countries' => $this->movie['production_countries'],
                'recommendations' => collect(isset($this->movie['recommendations']) ? $this->movie['recommendations']['results'] : [])->map(function($recommendations) {
                    return collect($recommendations)->only('id', 'title', 'overview', 'vote_average')->merge([
                        'recommendation_movie_id' => $recommendations['id'],
                        'movie_id' => $this->id,
                        'title' => $recommendations['title'],
                        'title_sort' => preg_replace("/[^A-Za-z0-9 ]/", '', str_replace(['The ', 'A ', 'An ', "'", '"'], ['','','','',''], $recommendations['title'])),
                        'backdrop' => $recommendations['backdrop_path'],
                        'poster' => $recommendations['poster_path'],
                    ])->except('poster_path','backdrop_path', 'id');
                }),
                'similars' => collect(isset($this->movie['similar']) ? $this->movie['similar']['results'] : [])->map(function($similar) {
                    return collect($similar)->merge([
                        'similar_movie_id' => $similar['id'],
                        'movie_id' => $this->id,
                        'title' => $similar['title'],
                        'title_sort' => preg_replace("/[^A-Za-z0-9 ]/", '', str_replace(['The ', 'A ', 'An ', "'", '"'], ['','','','',''], $similar['title'])),
                        'backdrop' => $similar['backdrop_path'],
                        'poster' => $similar['poster_path'],
                    ])->only('similar_movie_id','movie_id', 'title', 'title_sort', 'backdrop', 'poster', 'overview');
                }),
                'spoken_languages' => collect($this->movie['spoken_languages'])->pluck('iso_639_1'),
                'status' => $this->movie['status'],
                'translations' => collect(isset($this->movie['translations']) ? $this->movie['translations']['translations'] : [])->map(function($translations) {
                    return collect($translations)->merge([
                        'iso_639_1' => $translations['iso_639_1'],
                        'iso_3166_1' => $translations['iso_3166_1'],
                        'name' => $translations['name'],
                        'english_name' => $translations['english_name'],
                        'title' => $translations['data']['title'],
                        'overview' => $translations['data']['overview'],
                        'movie_id' => $this->movie['id']
                    ])->except('data');
                })->filter(function($i) {
                    return $i != null ? $i : null;
                }),
                'person_translations' => collect($this->translations)->filter(function($i) {
                    return $i != null ? $i : null;
                }),
                'videos' => $videos,
                'trailer'  => $trailer,
                'vote_average' => $this->movie['vote_average'],
                'vote_count' => $this->movie['vote_count'],
                'watch/providers' => collect(isset($this->movie['watch/providers']) ? $this->movie['watch/providers']['results'] : []),

            ])->except([
                'production_countries',
                'languages',
                'spoken_languages',
                'watch/providers',
                'vote_count',
                'credits',
                'original_name',
                'tagline',
                'type',
                'runtime',
            ])->toArray();

            ksort($list);
            // dd($list);

            Storage::put($file, json_encode($list));
            return $list;
        // });
    }
}
