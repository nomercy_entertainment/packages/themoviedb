<?php

namespace Nomercy\Themoviedb\Http\Controllers\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Nomercy\Themoviedb\Facades\Helper;
use Illuminate\Support\Facades\Storage;

class ClientEpisode extends Controller
{
    public $token, $id, $lang, $tv;

    public function __construct()
    {
        $this->token = env('TMDB_APIKEY');
    }

    /**
     * Fetches Episode response.
     *
     * @param  integer  $id
     * @param  integer  $season
     * @param  integer  $episode
     * @param  integer  $season_id
     * @return array
     */
    public function build($id, $season, $episode, $season_id = null){

        $this->id = $id;
        $this->season = $season;
        $this->episode = $episode;
        $this->season_id = $season_id;

        $key = '_client_episode_' . $id . '_S' . $season . '_S' . $episode;
        // return Cache::rememberForever($key, function () {

            $file = '/cache/client/tv/' . $this->id . '/' . $this->id . '_' . $this->season . '_' . $this->episode . '.json';
            if(Storage::exists($file)){
                return json_decode(utf8_decode(Storage::get($file)), true);
            }

            $append = [
                'content_ratings',
                'credits',
                'external_ids',
                'images',
                'translations',
            ];

            $this->translations = [];

            $this->episode = Http::withToken($this->token)
                ->get('https://api.themoviedb.org/3/tv/' . $this->id . '/season/' . $this->season . '/episode/' . $this->episode . '?append_to_response=' . implode(',', $append))
                ->json();

            if($this->episode != null && isset($this->episode['id']) && $this->episode['id'] != null){

                $image = collect($this->episode)->pluck('images')->map(function($posters) {
                    return collect($posters)->pluck('file_path')->first();
                });

                $list = collect($this->episode)->merge([
                    'air_date' => Carbon::createFromDate($this->episode['air_date'] ?? null)->format('Y-m-d'),
                    'still' => $image['stills'] ?? null,
                    'cast' => collect($this->episode['credits']['cast'])->map(function($credits){
                        return collect($credits)->merge([
                            'profile_id' => $credits['id'],
                            'known_for_department' => isset($credits['known_for_department']) ? $credits['known_for_department'] : null,
                        ])->except('id');
                    }),
                    'crew' => collect($this->episode['credits']['crew'])->map(function($credits){
                        return collect($credits)->merge([
                            'profile_id' => $credits['id'],
                            'known_for_department' => isset($credits['known_for_department']) ? $credits['known_for_department'] : null,
                        ])->except('id');
                    }),
                    'guest_stars' => collect(isset($this->episode['credits']['guest_stars']) ? $this->episode['credits']['guest_stars'] : [])->map(function($credits){
                        return collect($credits)->merge([
                            'profile_id' => $credits['id'],
                            'known_for_department' => isset($credits['known_for_department']) ? $credits['known_for_department'] : null,
                        ])->except('id');
                    }),
                    'id' => collect($this->episode)->only('id')->first(),
                    'images' => [
                        "stills" => collect($this->episode['images']['stills'])->map(function($stills) {
                            return collect($stills)->merge([
                                'tv_id' => $this->id,
                                'season_id' => $this->season_id,
                                'episode_id' => $this->episode['id']
                            ]);
                        }),
                    ],
                    "tvdb_id" => collect($this->episode['external_ids'])->only('tvdb_id')->first(),
                    "imdb_id" => collect($this->episode['external_ids'])->only('imdb_id')->first(),
                    "external_links" => [
                        "tvdb" => isset($this->episode['external_ids']) && isset($this->episode['external_ids']['tvdb_id']) ? 'https://thetvdb.com/?tab=series&id='. $this->episode['external_ids']['tvdb_id'] : null,
                    ],
                    'still' => collect($this->episode['images']['stills'])->first()['file_path'] ?? null,
                    'title' => $this->episode['name'],
                    'overview' =>$this->episode['overview'],
                    'translations' => collect(isset($this->episode['translations']) ? $this->episode['translations']['translations'] : [])->map(function($translations) {
                        if($translations['data']['name'] != '' || $translations['data']['overview'] != ''){
                            return collect($translations)->merge([
                                'iso_639_1' => $translations['iso_639_1'],
                                'iso_3166_1' => $translations['iso_3166_1'],
                                'name' => $translations['name'],
                                'english_name' => $translations['english_name'],
                                'title' => $translations['data']['name'],
                                'overview' => $translations['data']['overview'],
                                'tv_id' => $this->id,
                                'season_id' => $this->season_id,
                                'episode_id' => $this->episode['id'],
                            ])->except('data');
                        }
                        })->filter(function($i) {
                            return $i != null ? $i : null;
                    }),
                    'person_translations' => collect($this->translations)->filter(function($i) {
                        return $i != null ? $i : null;
                    }),
                    'tv_id' => $this->id,
                    'season_id' => $this->season_id,
                    'seaon_number' => !isset($this->episode['season_number']) || ($this->episode['season_number'] != '' && $this->episode['season_number'] != null) ? $this->episode['season_number'] : 0

                ])->except([
                    '_id',
                    'name',
                    'still_path',
                ])->toArray();

                ksort($list);
                // dd($list);

                Storage::put($file, json_encode($list));
                return $list;
            }
            else {
                return [];
            }
        // });
    }
}
