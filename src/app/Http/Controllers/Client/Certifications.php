<?php

namespace Nomercy\Themoviedb\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Nomercy\Themoviedb\Models\Certification;
use Torann\GeoIP\Facades\GeoIP;

class ClientCertifications extends Controller
{
    public $certifications;

    public function __construct()
    {

        $this->token = env('TMDB_APIKEY');
        $file = '/cache/certifications.json';
        $languages = new ClientLanguage();

        if(!Storage::exists($file)){

            $this->certifications = [];
            $allCertifications = [];
            // foreach($languages->get() as $this->lang){' ## If the moviedb releases translated classification, for now we need to do it with a translation file.
                // $lang = $this->lang['iso_639_1'];

                $tv = Http::withToken($this->token)
                    ->get('https://api.themoviedb.org/3/certification/tv/list?language=en')
                    ->json()['certifications'];

                $movie = Http::withToken($this->token)
                    ->get('https://api.themoviedb.org/3/certification/tv/list?language=en')
                    ->json()['certifications'];

                $allCertifications = array_merge_recursive($tv, $movie);

                $allCertifications = collect($allCertifications)->map(function($certifications, $lang) {
                    $this->lang = $lang;
                    return collect($certifications)->map(function($certification) {
                        return collect($certification)->merge([
                            'iso_3166_1' => $this->lang,
                            'rating' => $certification['certification'],
                        ])->except('certification');
                    });
                 })->toArray();

            // }

            foreach($allCertifications as $certifications){
                foreach($certifications as $certification){
                    if($certification['rating'] != null){
                        $this->certifications[] = $certification;
                    }
                }
            }

            Storage::put($file, json_encode($this->certifications));
        }
        else {
            $this->certifications = json_decode(Storage::get($file), true);
        }

        foreach(array_chunk($this->certifications, 2000) as $transaction){
            Certification::upsert((array) $transaction, "id");
        }

        return $this->certifications;

    }

    /**
     * Fetches all certifications.
     *
     * @return array
     * @return DatabaseFiller
     */
    public function get()
    {
        return collect($this->certifications)->where('iso_3166_1', session('country'))->toArray();
    }
}
