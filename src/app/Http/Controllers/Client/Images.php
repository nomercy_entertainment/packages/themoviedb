<?php

namespace Nomercy\Themoviedb\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Nomercy\Themoviedb\Facades\Helper;

class ClientImages extends Controller
{
    private $result;

    public function __construct($response){

        $this->images = [];

        collect($response)->map(function($season) {
            return collect($season)->only('images')->map(function($img) {
                if(isset($img['posters'])){
                    foreach($img['posters'] as $i){
                        $this->images['images']['posters'][] = collect($i)->toArray();
                    }
                }
                if(isset($img['stills'])){
                    foreach($img['stills'] as $i){
                        $this->images['images']['stills'][] = collect($i)->toArray();
                    }
                }
                return collect($img)->toArray();
            })->toArray();
        })->toArray();

        $this->images = array_merge_recursive(collect($response)->only(
            "backdrop",
            "backdrop_path",
            "poster",
            "posters",
            "poster_path",
            "images",
        )->toArray(), $this->images);

        return $this;

    }

    /**
     * Database data.
     *
     * @return array
     */
    public function table(){

        $result = [];

        if (isset($this->images['images'])){
            foreach($this->images['images'] as $key => $images){
                foreach($images as $image){
                    $result[] = collect($image)->merge([
                        'type' => preg_replace('/s$/', '', $key)
                    ]);
                }
            }
        }

        return collect($result)->unique('file_path')->toArray();
    }

    /**
     * list of all images to encode.
     *
     * @return array
     */
    public function ffmpeg(){

        $this->download = [];

        // if(env('DOWNLOAD_ALL_IMAGES', true)){

            if(isset($this->images['images']['backdrops'])){
                foreach($this->images['images']['backdrops'] as $key => $backdrop){
                    $this->download[] = [$backdrop['file_path'], 'original'];
                }
            }
            if(isset($this->images['images']['posters'])){
                foreach($this->images['images']['posters'] as $key => $poster){
                    $this->download[] = [$poster['file_path'], 'original'];
                    $this->download[] = [$poster['file_path'], 'w300'];
                }
            }
        // }
        if(isset($this->images['images']['stills'])){
            foreach($this->images['images']['stills'] as $key => $still){
                $this->download[] = [$still['file_path'], 'original'];
                $this->download[] = [$still['file_path'], 'w300'];
            }
        }
        if (isset($this->images)){
            foreach($this->images as $key => $image){
                if(Helper::contains($key, 'backdrop') == true){
                    $this->download[] = [$image, 'original'];
                }

                if(Helper::contains($key, 'poster') == true){
                    $this->download[] = [$image, 'original'];
                    $this->download[] = [$image, 'w300'];
                }
            }
        }

        return collect($this->download)->unique();
    }

    public function toArray(){
        return collect($this)->toArray();
    }
}
