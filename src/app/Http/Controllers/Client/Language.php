<?php

namespace Nomercy\Themoviedb\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Nomercy\Themoviedb\Models\Genre as ModelsGenre;
use Illuminate\Support\Facades\Http;
use Nomercy\Themoviedb\Models\Language;

class ClientLanguage extends Controller
{
    public $languages;

    public function __construct()
    {
        $token = env('TMDB_APIKEY');

        $this->languages = Http::withToken($token)
            ->get('https://api.themoviedb.org/3/configuration/languages')
            ->json();

        Language::upsert($this->languages, 'iso_639_1');

        return $this->languages;
    }

    /**
     * Fetches Available languages.
     *
     * @return array
     * @return DatabaseFiller
     */
    public function get()
    {
        return collect($this->languages)->toArray();
    }
}
