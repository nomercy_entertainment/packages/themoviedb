<?php

namespace Nomercy\Themoviedb\Http\Controllers\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Nomercy\Themoviedb\Facades\Helper;
use Illuminate\Support\Facades\Storage;

class ClientSeason extends Controller
{
    public $token, $id, $lang, $tv;

    public function __construct($lang = null)
    {
        $this->token = env('TMDB_APIKEY');
    }

    /**
     * Fetches Season response.
     *
     * @param  integer  $id
     * @param  integer  $season
     * @return array
     */
    public function build($id, $season){
        $this->id = $id;
        $this->season = $season;

        $key = '_client_season_' . $id . '_S' . $season;
        // return Cache::rememberForever($key, function () {

            $file = '/cache/client/tv/' . $this->id . '/' . $this->id . '_' . $this->season .  '.json';
            if(Storage::exists($file)){
                return json_decode(utf8_decode(Storage::get($file)), true);
            }

            $append = [
                'credits',
                'external_ids',
                'images',
                'translations',
            ];
            $this->translations = [];

            $this->season = Http::withToken($this->token)
                ->get('https://api.themoviedb.org/3/tv/' . $this->id . '/season/' . $this->season . '?append_to_response=' . implode(',', $append))
                ->json();

            if(!isset($this->season['id'])){
                return $this->season;
            }

            $image = collect($this->season['images']['posters'])->map(function($posters) {
                $data = collect($posters)->where('iso_639_1', '==', app()->getLocale())->pluck('file_path')->first();
                if($data == null){
                    $data = collect($posters)->pluck('file_path')->first();
                }
                return $data;
            })->first();

            $list = collect($this->season)->merge([
                'air_date' => Carbon::createFromDate($this->season['air_date'])->format('Y-m-d'),
                'poster' => $image ?? $this->season['poster_path'],
                'cast' => collect($this->season['credits']['cast'])->map(function($credits){
                    return collect($credits)->merge([
                        'profile_id' => $credits['id'],
                        'known_for_department' => isset($credits['known_for_department']) ? $credits['known_for_department'] : null,
                    ])->except('id');
                }),
                'crew' => collect($this->season['credits']['crew'])->map(function($credits){
                    return collect($credits)->merge([
                        'profile_id' => $credits['id'],
                        'known_for_department' => isset($credits['known_for_department']) ? $credits['known_for_department'] : null,
                    ])->except('id');
                }),
                'id' => collect($this->season)->only('id')->first(),
                'images' => [
                    "posters" => collect($this->season['images']['posters'])->map(function($posters) {
                        return collect($posters)->merge([
                            'tv_id' => $this->id,
                            'season_id' => $this->season['id']
                        ]);
                    }),
                ],
                "tvdb_id" => collect($this->season['external_ids'])->only('tvdb_id')->first(),
                "external_links" => [
                    "tvdb" => 'https://thetvdb.com/?tab=series&id='. $this->season['external_ids']['tvdb_id'],
                ],
                "episodes" => collect($this->season['episodes'])->map(function($episode){
                    return collect($episode)->merge([
                        'season_number' => $episode['season_number'],
                        'episode_number' => $episode['episode_number'],
                    ])->only('season_number','episode_number');
                }),
                'title' => $this->season['name'],
                'overview' => $this->season['overview'],
                'translations' => collect(isset($this->season['translations']) ? $this->season['translations']['translations'] : [])->map(function($translations) {
                    if($translations['data']['name'] != '' || $translations['data']['overview'] != ''){
                        return collect($translations)->merge([
                            'iso_639_1' => $translations['iso_639_1'],
                            'iso_3166_1' => $translations['iso_3166_1'],
                            'name' => $translations['name'],
                            'english_name' => $translations['english_name'],
                            'title' => $translations['data']['name'],
                            'overview' => $translations['data']['overview'],
                            'tv_id' => $this->id,
                            'season_id' => $this->season['id'],
                        ])->except('data');
                    }
                })->filter(function($i) {
                    return $i != null ? $i : null;
                }),
                'person_translations' => collect($this->translations)->filter(function($i) {
                    return $i != null ? $i : null;
                }),
                'tv_id' => $this->id

            ])->except([
                '_id',
                'name',
                'credits',
                'poster_path',
            ])->toArray();

            ksort($list);
            // dd($list);

            Storage::put($file, json_encode($list));
            return $list;
        // });

    }
}
