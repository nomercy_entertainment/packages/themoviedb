<?php

namespace Nomercy\Themoviedb\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Nomercy\Themoviedb\Models\Genre;
use Illuminate\Support\Facades\Storage;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientLanguage;

class ClientGenre extends Controller
{
    public $certifications;

    public function __construct()
    {
        $this->token = env('TMDB_APIKEY');
        $file = '/cache/genres.json';
        $languages = new ClientLanguage();

        if(!Storage::exists($file)){

            $this->genres = [];
            $allGenres = [];
            foreach($languages->get() as $this->lang){

                $tv = Http::withToken($this->token)
                    ->get('https://api.themoviedb.org/3/genre/tv/list?language=' .  $this->lang['iso_639_1'])
                    ->json()['genres'];

                $movie = Http::withToken($this->token)
                    ->get('https://api.themoviedb.org/3/genre/movie/list?language=' .  $this->lang['iso_639_1'])
                    ->json()['genres'];

                $allGenres[] = collect(array_merge_recursive($tv, $movie))->map(function($language) {
                    return collect($language)->merge([
                        'iso_639_1' => $this->lang['iso_639_1'],
                        'genre_id' => $language['id']
                    ])->except('id');
                })->toArray();
            }

            foreach($allGenres as $genres){
                foreach($genres as $genre){
                    if($genre['name'] != null){
                        $this->genres[] = $genre;
                    }
                }
            }

            Storage::put($file, json_encode($this->genres));
        }
        else {

            $this->genres = json_decode(Storage::get($file), true);

        }

        Genre::upsert($this->genres, 'iso_639_1');

        return $this->genres;
    }

    /**
     * Fetches all genres.
     *
     * @return array
     * @return DatabaseFiller
     */
    public function get()
    {
        return collect($this->genres)->toArray();
    }
}
