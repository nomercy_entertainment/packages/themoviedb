<?php

namespace Nomercy\Themoviedb\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Nomercy\Themoviedb\Models\Tv;
use App\Http\Controllers\Controller;

use Nomercy\Themoviedb\Models\Movie;
use Nomercy\Themoviedb\Facades\Helper;

use Nomercy\Themoviedb\Facades\Logger;
use Illuminate\Support\Facades\Storage;
use Nomercy\Themoviedb\Jobs\DownloadTVData;
use Nomercy\Themoviedb\Jobs\DownloadMovieData;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientTv;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientGenre;

use Nomercy\Themoviedb\Http\Controllers\Client\ClientMovie;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientSeason;

use Nomercy\Themoviedb\Http\Controllers\Database\DatabaseTv;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientEpisode;
use Nomercy\Themoviedb\Http\Controllers\Database\DatabaseMovie;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientCertifications;

class ThemoviedbController extends Controller
{

    public $token, $id, $lang, $tv, $i;

    public function __construct()
    {
        $this->token = env('TMDB_APIKEY');
    }

    public function tv($id = null)
    {

        $id = $this->id ?? $id;
        $tvClient = new ClientTv();
        $data = $tvClient->build($id);

        return $data;
    }

    public function season($id = null, $season = null)
    {
        $id = $this->id ?? $id;
        $season = $this->season ?? $season;

        $seasonClient = new ClientSeason();
        $data = $seasonClient->build($id, $season);

        return $data;
    }

    public function episode($id = null, $season = null, $episode = null)
    {
        $id = $this->id ?? $id;
        $season = $this->season ?? $season;
        $episode = $this->episode ?? $episode;

        $episodeClient = new ClientEpisode();
        $data = $episodeClient->build($id, $season, $episode);

        return $data;
    }

    public function movie($id = null)
    {
        $id = $this->id ?? $id;

        $movieClient = new ClientMovie();
        $data = $movieClient->build($this->token, $this->id);

        return $data;
    }

    public function genres()
    {
        $genres = (new ClientGenre())->get();
        $data = $genres;

        return $data;
    }

    public function certifications()
    {

        $certifications = (new ClientCertifications())->get();
        $data = $certifications;

        return $data;
    }

    public function certification_translations()
    {

        $list = [];
        foreach ((new ClientCertifications())->get() as $item) {
            $list[$item['iso_3166_1']][$item['rating']] = $item['meaning'];
        }

        if (count($list) == 0) {
            abort(404);
        }
        $data = $list;

        return $data;
    }


    public function build_tv($id = null)
    {

        $this->id = $id;

        $file = '/cache/build/tv/' . $id . '.json';
        $update = Helper::checkUpdate('tv', $id);
        // $update = false;

        if ($update) {
            $tvClient = new ClientTv();
            $tv = $tvClient->build($id);

            if (isset($tv['seasons']) && count($tv['seasons']) > 0) {
                $res = collect($tv)->merge([
                    'seasons' => collect($tv['seasons'])->map(function ($season) use($id){
                        $season = (new ClientSeason())->build($id, $season);
                        if (isset($season['episodes']) && count($season['episodes']) > 0) {
                            return collect($season)->merge([
                                'episodes' => collect($season['episodes'])->map(function ($episode) use($id, $season) {
                                    $episode = (new ClientEpisode())->build($id, $episode['season_number'], $episode['episode_number'], $season['id']);
                                    return collect($episode);
                                }),
                            ]);
                        }
                    }),
                ]);

                Storage::put($file, json_encode($res));

                return $res;
            } else {
                return $tv;
            }
        }
        else if (Storage::exists($file)) {
            $data = json_decode(utf8_decode(Storage::get($file)), true);
            return $data;
        }
    }

    public function store_tv($id)
    {

        $tv = $this->build_tv($id);
        $build = (new DatabaseTv())->build($tv);

        return $build;
    }

    public function build_movie($id = null)
    {

        $this->id = $id;

        $file = '/cache/build/movie/' . $id . '.json';
        $update = Helper::checkUpdate('movie', $id);
        // $update = false;

        if ($update) {
            $movieClient = new Clientmovie();
            $movie = $movieClient->build($id);
            $res = collect($movie)->merge([]);

            Storage::put($file, json_encode($res));

            return $res;
        } else if (Storage::exists($file)) {
            $data = json_decode(utf8_decode(Storage::get($file)), true);
            return $data;
        }
    }

    function store_movie($id)
    {

        $movie = $this->build_movie($id);
        $build = (new DatabaseMovie())->build($movie);

        return $build;
    }


    public function loop_all_tv($reverse = false)
    {
        app()->setlocale(config('themoviedb.default_language'));

        $startTime = Carbon::now();

        $processed = 0;

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        // $skip = array(923, 1126, 1832, 113038);
        $skip = array();

        $ids = Tv::select('id')->get()->pluck('id')->toArray();
        $ids = array();
        if ($reverse == false) {
            for ($i = 1; $i < 200000; $i++) {
                if (!in_array($i, $skip) && !in_array($i, $ids)) {
                    Logger::info('Adding show ' . $i);
                    $processed++;

                    $dlTmdbData = (new DownloadTVData($i));
                    dispatch($dlTmdbData)->onQueue('default');

                }
            }
        }
        else {
            for ($i = 200000; $i > 1; $i--) {
                if (!in_array($i, $skip) && !in_array($i, $ids)) {
                    Logger::info('Adding show ' . $i);
                    $processed++;

                    $dlTmdbData = (new DownloadTVData($i));
                    dispatch($dlTmdbData)->onQueue('default');

                }
            }
        }

        app()->setlocale(config('app.locale'));
        $duration = Carbon::now()->diffAsCarbonInterval($startTime);
        Logger::info('All ' . $processed . ' shows added and it took:  ' . $duration);
    }

    public function get_tv($items = null, Request $request = null)
    {
        app()->setlocale(config('themoviedb.default_language'));

        $startTime = Carbon::now();

        $processed = 0;

        if ($request->items ?? null != null || $items != null) {
            $items = $request->items ?? $items;

            if (!is_array($items)) {
                $items = explode(',', $items);
            }
            foreach ($items as $i) {
                Logger::info('Adding show ' . $i);
                $processed++;

                $dlTmdbData = (new DownloadTVData($i));
                dispatch($dlTmdbData)->onQueue('default');
            }
        }

        app()->setlocale(config('app.locale'));
        $duration = Carbon::now()->diffAsCarbonInterval($startTime);
        Logger::info('All ' . $processed . ' shows added and it took:  ' . $duration);
    }


    public function loop_all_movie($reverse = false)
    {

        app()->setlocale(config('themoviedb.default_language'));

        $processed = 0;
        $startTime = Carbon::now();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        $skip = array();

        $ids = Movie::select('id')->get()->pluck('id')->toArray();
        $ids = array();
        if ($reverse == false) {
            for ($i = 1; $i < 800000; $i++) {
                if (!in_array($i, $skip) && !in_array($i, $ids)) {
                    Logger::info('Adding movie ' . $i);
                    $processed++;

                    $dlTmdbData = (new DownloadMovieData($i));
                    dispatch($dlTmdbData)->onQueue('default');
                }
            }
        }
        else {
            for ($i = 800000; $i > 1; $i--) {
                if (!in_array($i, $skip) && !in_array($i, $ids)) {
                    Logger::info('Adding movie ' . $i);
                    $processed++;

                    $dlTmdbData = (new DownloadMovieData($i));
                    dispatch($dlTmdbData)->onQueue('default');

                }
            }
        }

        app()->setlocale(config('app.locale'));
        $duration = Carbon::now()->diffAsCarbonInterval($startTime);
        Logger::info('All ' . $processed . ' movies added and it took:  ' . $duration);
    }

    public function get_movies($items = null, Request $request = null)
    {
        app()->setlocale(config('themoviedb.default_language'));

        $processed = 0;
        $startTime = Carbon::now();

        if ($request->items ?? null != null || $items != null) {
            $items = $request->items ?? $items;

            if (!is_array($items)) {
                $items = explode(',', $items);
            }
            foreach ($items as $i) {
                Logger::info('Adding movie ' . $i);
                $processed++;

                $dlTmdbData = (new DownloadMovieData($i));
                dispatch($dlTmdbData)->onQueue('default');
            }
        }

        app()->setlocale(config('app.locale'));
        $duration = Carbon::now()->diffAsCarbonInterval($startTime);
        Logger::info('All ' . $processed . ' movies added and it took:  ' . $duration);
    }
}
