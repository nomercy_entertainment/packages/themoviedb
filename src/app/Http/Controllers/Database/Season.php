<?php

namespace Nomercy\Themoviedb\Http\Controllers\Database;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use Nomercy\Themoviedb\Models\Tv;
use Nomercy\Themoviedb\Models\Cast;
use Nomercy\Themoviedb\Models\Crew;
use Nomercy\Themoviedb\Models\Media;
use Nomercy\Themoviedb\Models\Season;
use Nomercy\Themoviedb\Facades\Logger;
use Nomercy\Themoviedb\Models\Translation;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientImages;

class DatabaseSeason extends Controller
{
    protected $season;
    public $imagesArray;

    /**
     * Inserts all Season data to database.
     *
     * @return array
     * @return DatabaseFiller
     */
    public function __construct($seasons)
    {
        $this->seasons = $seasons;
        $this->imagesArray = [];

        $dbSeasons = collect($seasons)->map(function($season) {
            return collect($season)->only(
                'air_date',
                'id',
                'overview',
                'poster',
                'season_number',
                'title',
                'tv_id',
                'tvdb_id
            ');
        })->unique('id')->toArray();

        if(isset($dbSeasons[0]['tv_id'])){
            $this->tv = $dbSeasons[0]['tv_id'];

            foreach($dbSeasons as $seas){
                // Logger::info('Adding tv ' . $this->tv . ' season ' . $seas['id'] . ' to the database');
                if(isset($seas['id'])){
                    Season::updateOrCreate(['id' => $seas['id']], $seas);
                }
                // Season::upsert((array) $transaction, "id");
            }

            foreach($seasons as $season){
                if(isset($season['id'])){
                    $modelSeason = Season::find($season['id']);
                }
                else {
                    $modelSeason = null;
                }

                if($modelSeason != null){
                    // Logger::info('Adding tv ' . $this->tv . ' season ' . $season['id'] . ' other data to the database');
                    $this->seasonId = $season['id'];
                    $imagesArray = [];

                    if(isset($season['cast'])){
                        $casts = [];
                        foreach($season['cast'] as $cast){
                            Cast::updateOrCreate(['credit_id' => $cast['credit_id']], collect($cast)->except('id')->toArray());
                            $casts[] = $cast;
                        }
                        // Cast::upsert($casts, 'credit_id');
                        $c = Cast::wherein('credit_id', collect($casts)->pluck('credit_id')->unique())->pluck('id');
                        $modelSeason->cast()->sync($c);
                    }

                    if(isset($season['crew'])){
                        $crews = [];
                        foreach($season['crew'] as $crew){
                            Crew::updateOrCreate(['credit_id' => $crew['credit_id']], collect($crew)->except('id')->toArray());
                            $crews[] = $crew;
                        }
                        // Crew::upsert($crews, 'credit_id');
                        $c = Crew::wherein('credit_id', collect($crews)->pluck('credit_id')->unique())->pluck('id');
                        $modelSeason->crew()->sync($c);
                    }

                    if(isset($season['translations'])){
                        $translations = collect($season['translations'])->toArray();
                        Translation::upsert($translations, ['tv_id', 'season_id', 'episode_id', 'iso_3166_1']);
                    }

                    if(isset($season['person_translations'])){
                        $translations = collect($season['person_translations'])->toArray();
                        Translation::upsert($translations, ['tv_id', 'iso_639_1']);
                    }

                    $images = (new ClientImages($season));

                    $images = collect($images->table())->map(function($image) {
                        return collect($image)->merge([
                            'season_id' => $this->seasonId
                        ]);
                    })->toArray();

                    Media::upsert($images, 'file_path');

                    foreach($images as $img){
                        $imagesArray[] = $img;
                    }
                }
            }

            foreach($imagesArray as $img){
                $this->imagesArray[] = $img;
            }

            return $this->imagesArray;
        }
    }

    /**
     * Returns all images.
     *
     * @return collection
     */
    public function images(){
        return $this->imagesArray;
    }

    public function toArray(){
        return collect($this)->toArray();
    }




}
