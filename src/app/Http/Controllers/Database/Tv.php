<?php

namespace Nomercy\Themoviedb\Http\Controllers\Database;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Nomercy\Themoviedb\Models\Tv;
use Nomercy\Themoviedb\Models\Cast;
use Nomercy\Themoviedb\Models\Crew;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Nomercy\Themoviedb\Models\Genre;
use Nomercy\Themoviedb\Models\Media;
use Nomercy\Themoviedb\Facades\Helper;

use Nomercy\Themoviedb\Facades\Logger;
use Nomercy\Themoviedb\Models\Creator;
use Nomercy\Themoviedb\Models\Similar;
use Nomercy\Themoviedb\Models\Translation;
use Nomercy\Themoviedb\Models\Certification;
use Nomercy\Themoviedb\Models\Recommendation;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientGenre;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientImages;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientCertifications;

class DatabaseTv extends Controller
{
    public $imagesArray;

    /**
     * Inserts all tv data to database.
     *
     * @return array
     * @return DatabaseFiller
     */
    public function build($tv, $forceUPdate = false){

        $this->tv = $tv;

        if(isset($tv['id']) &&  $tv['id'] != null){

            if(Genre::count() < 1){
                new ClientGenre();
            }

            if(Certification::count() < 1){
                new ClientCertifications();
            }
            $dbTv = collect($tv)->only(
                "backdrop",
                "duration",
                "first_air_date",
                "homepage",
                "id",
                "in_production",
                "last_air_date",
                "last_episode_to_air",
                "name",
                "next_episode_to_air",
                "number_of_episodes",
                "number_of_seasons",
                "origin_country",
                "original_language",
                "overview",
                "popularity",
                "poster",
                "status",
                "title",
                "title_sort",
                "trailer",
                "vote_average"
            )->toArray();

            if($dbTv != []){
                $now = Carbon::now();
                $diff = Carbon::createFromTimestamp(Tv::where('id', $tv['id'])->first()->updated_at ?? Carbon::now()->subYear(1)->toDateString() )->diffInDays($now);

                // if($diff > config('themoviedb.not_update_before') || $forceUPdate){

                    Logger::info('Adding tv ' . $tv['id'] . ' Adding tv to the database');
                    Tv::upsert($dbTv, 'id');

                    $modelTv = Tv::find($tv['id']);

                    if($modelTv != null){

                        if(isset($tv['created_by'])){
                            $creators = [];
                            foreach($tv['created_by'] as $creator){
                                Creator::updateOrCreate(['credit_id' => $creator['credit_id']], collect($creator)->except('id')->toArray());
                                $creators[] = $creator;
                            }
                            // Creator::upsert($creators, 'id');
                            $c = Creator::wherein('credit_id', collect($creators)->pluck('credit_id')->unique())->pluck('id');
                            $modelTv->creator()->sync($c);
                        }

                        if(isset($tv['cast'])){
                            $casts = [];
                            foreach($tv['cast'] as $cast){
                                Cast::updateOrCreate(['credit_id' => $cast['credit_id']], collect($cast)->except('id')->toArray());
                                $casts[] = $cast;
                            }
                            // Cast::upsert($casts, 'credit_id');
                            $c = Cast::wherein('credit_id', collect($casts)->pluck('credit_id')->unique())->pluck('id');
                            $modelTv->cast()->sync($c);
                        }

                        if(isset($tv['crew'])){
                            $crews = [];
                            foreach($tv['crew'] as $crew){
                                Crew::updateOrCreate(['credit_id' => $crew['credit_id']], collect($crew)->except('id')->toArray());
                                $crews[] = $crew;
                            }
                            // Crew::upsert($crews, 'credit_id');
                            $c = Crew::wherein('credit_id', collect($crews)->pluck('credit_id')->unique())->pluck('id');
                            $modelTv->crew()->sync($c);
                        }


                        if(isset($tv['genres'])){
                            $modelGenres = Genre::whereIn('genre_id', $tv['genres'])->get()->pluck('id')->unique();
                            $modelTv->genres()->sync($modelGenres);
                        }


                        if(isset($tv['translations'])){
                            $translations = collect($tv['translations'])->toArray();
                            Translation::upsert($translations, ['tv_id', 'season_id', 'episode_id', 'iso_3166_1']);
                        }

                        if(isset($tv['person_translations'])){
                            $translations = collect($tv['person_translations'])->toArray();
                            Translation::upsert($translations, ['tv_id', 'iso_639_1']);
                        }

                        if(isset($tv['videos'])){
                            $videos = collect($tv['videos'])->map(function($video) {
                                return collect($video)->merge([
                                    'tv_id' => $this->tv['id']
                                ]);
                            })->toArray();
                            Media::upsert($videos, ['tv_id', 'iso_639_1']);

                        }

                        if(config('themoviedb.download_trailers')){
                            foreach ($tv['videos'] as $item) {
                                if ($item['type'] == 'Trailer') {
                                    Logger::info('Adding tv ' . $tv['id'] . ' Downloading trailer ' . $item['key']);

                                    $cmd = Helper::youtubeDl() . " -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --geo-bypass --quiet --merge-output-format mp4 " . $item['key'] . ' -o  "/var/www/html/storage/app/assets/trailer/tv/' . $tv['id'] . '.trailer.%(ext)s" --ffmpeg-location '. Helper::ffmpeg();
                                    exec($cmd, $output);

                                    Logger::info('Adding tv ' . $tv['id'] . collect($output)->merge($output)->except([3, 4, 5, 6]));
                                }

                                if (file_exists("/assets/trailer/" . $tv['id'] . ".trailer.mp4")) {
                                    break;
                                }
                            }

                            foreach ($tv['videos'] as $item) {
                                if (!file_exists(storage_path('app/assets/trailer/tv/' . $tv['id'] . '.trailer.mp4'))) {
                                    Logger::info('Adding tv ' . $tv['id'] . ' Downloading backup trailer ' . $item['key']);

                                    $cmd = Helper::youtubeDl() . " -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --geo-bypass --quiet --merge-output-format mp4 " . $item['key'] . ' -o  "/var/www/html/storage/app/assets/trailer/tv/' . $tv['id'] . '.trailer.%(ext)s" --ffmpeg-location '. Helper::ffmpeg();
                                    exec($cmd, $output);

                                    Logger::info('Adding tv ' . $tv['id'] . collect($output)->merge($output)->except([3, 4, 5, 6]));
                                }
                            }
                        }

                        // if(isset($tv['recommendations'])){
                        //     $list = collect($tv['recommendations'])->unique('tv_id')->toArray();
                        //     foreach ($list as $item){
                        //         Recommendation::insert($item);
                        //     }
                        //     // Recommendation::upsert($list, 'recommendation_tv_id');

                        // };

                        // if(isset($tv['similars'])){
                        //     $list = collect($tv['similars'])->unique('tv_id')->toArray();
                        //     foreach ($list as $item){
                        //         Similar::insert($item);
                        //     }
                        //     // Similar::upsert($list, 'similar_tv_id');
                        // };


                        if(isset($tv['recommendations'])){
                            foreach($tv['recommendations'] as $item){
                                if(tv::where('id', $item['recommendation_tv_id'])->count() != 0){
                                    Recommendation::updateOrCreate(['recommendation_tv_id' => $item['recommendation_tv_id']], $item);
                                }
                            }
                        }
                        if(isset($tv['similars'])){
                            foreach($tv['similars'] as $item){
                                if(tv::where('id', $item['similar_tv_id'])->count() != 0){
                                    Similar::updateOrCreate(['similar_tv_id' => $item['similar_tv_id']], $item);
                                }
                            }
                        }


                        // $alternativeTitles = collect($tv)->only('alternative_titles')->first();
                        // $externalIds = collect($tv)->only('external_ids')->first();
                        // $externalLinks = collect($tv)->only('external_links')->first();
                        // $keywords = collect($tv)->only('keywords')->first();
                        // $networks = collect($tv)->only('networks')->first();
                        // $productionCompanies = collect($tv)->only('production_companies')->first();
                        // $contentRatings = collect($tv)->only('content_ratings')->first();
                        // $reviews = collect($tv)->only('reviews')->first();
                        // $providers = collect($movie)->only('watch/providers')->first();


                        $seasons = collect($tv)->only('seasons')->first();
                        if($seasons != null && count($seasons) > 0){

                            $arr = collect(collect($tv)->only('seasons')->first())->map(function($seasons) {
                                if(isset($seasons['episodes'])){
                                    return collect($seasons['episodes'])->map(function($episodes) {
                                        return collect($episodes);
                                    });
                                }
                            })->toArray();

                            $episodes = [];
                            foreach($arr as $ep){
                                if($ep != null){
                                    foreach($ep as  $episode){
                                        $episodes[] = $episode;

                                    }
                                }
                            }

                            $t = new ClientImages($tv);
                            Media::upsert($t->table(), 'file_path');

                            new DatabaseSeason($seasons);
                            $s = new ClientImages($seasons);
                            if($s != null){
                                Media::upsert($s->table(), 'file_path');
                            }

                            new DatabaseEpisode($episodes);
                            $e = new ClientImages($episodes);
                            if($e != null){
                                Media::upsert($e->table(), 'file_path');
                            }

                            if(config('themoviedb.download_images')){
                                Logger::info('Adding tv ' . $tv['id'] . ' Downloading show images');
                                Helper::download($t->ffmpeg(), 'tv');
                                Helper::download($s->ffmpeg(), 'tv');
                                Helper::download($e->ffmpeg(), 'tv');
                            }
                        }
                    }
                // }
            }
            return $tv;
        }
    }

    public function toArray(){
        return collect($this)->toArray();
    }
}
