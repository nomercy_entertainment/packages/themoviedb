<?php

namespace Nomercy\Themoviedb\Http\Controllers\Database;

use Nomercy\Themoviedb\Models\Cast;
use Nomercy\Themoviedb\Models\Crew;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Nomercy\Themoviedb\Models\Media;

use Nomercy\Themoviedb\Facades\Logger;
use Nomercy\Themoviedb\Models\Episode;
use Nomercy\Themoviedb\Models\GuestStar;
use Nomercy\Themoviedb\Models\ExternalIds;
use Nomercy\Themoviedb\Models\Translation;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientImages;

class DatabaseEpisode extends Controller
{
    protected $episodes;
    public $imagesArray;

    /**
     * Inserts all Episode data to database.
     *
     * @return array
     * @return DatabaseFiller
     */
    public function __construct($episodes)
    {

        $this->episodes = $episodes;

        $dbEpisode = collect($episodes)->map(function($episode) {
            return collect($episode)->only(
                'air_date',
                'episode_number',
                'id',
                'imdb_id',
                'overview',
                'production_code',
                'season_id',
                'season_number',
                'still',
                'title',
                'tv_id',
                'tvdb_id',
                'vote_average',
                'vote_count'
            );
        })->unique('id')->toArray();

        foreach($dbEpisode as $ep){
            // Logger::info('Adding tv ' . $ep['tv_id'] . ' episode ' . $ep['id'] . ' to the database');
            if(isset($ep['season_number']) && $ep['season_number'] != null && $ep['season_number'] != ''){
                Episode::updateOrCreate(['id' => $ep['id']], collect($ep)->toArray());
            }
            else if(isset($ep['id'])){
                $ep['season_number'] = 0;
                Episode::updateOrCreate(['id' => $ep['id']], collect($ep)->toArray());
            }
        }

        $imagesArray = [];

        foreach($episodes as $episode){
            $modelepisode = Episode::find($episode['id']);

            // if($modelepisode != null){
                // Logger::info('Adding tv ' . $ep['tv_id'] . ' episode ' . $ep['id'] . ' other data to the database');
                if(isset($episode['cast'])){
                    $casts = [];
                    foreach($episode['cast'] as $cast){
                        Cast::updateOrCreate(['credit_id' => $cast['credit_id']], collect($cast)->except('id')->toArray());
                        $casts[] = $cast;
                    }
                    // Cast::upsert(collect($casts)->unique('credit_id')->toArray(), 'credit_id');
                    $c = Cast::wherein('credit_id', collect($casts)->pluck('credit_id')->unique())->pluck('id')->unique();
                    $modelepisode->cast()->sync($c);
                }

                if(isset($episode['crew'])){
                    $crews = [];
                    foreach($episode['crew'] as $crew){
                        Crew::updateOrCreate(['credit_id' => $crew['credit_id']], collect($crew)->except('id')->toArray());
                        $crews[] = $crew;
                    }
                    // Crew::upsert(collect($crews)->unique('credit_id')->toArray(), 'credit_id');
                    $c = Crew::wherein('credit_id', collect($crews)->pluck('credit_id')->unique())->pluck('id')->unique();
                    $modelepisode->crew()->sync($c);
                }

                if(isset($episode['guest_stars'])){
                    $guestStars = [];
                    foreach($episode['guest_stars'] as $guestStar){
                        GuestStar::updateOrCreate(['credit_id' => $guestStar['credit_id']], collect($guestStar)->except('id')->toArray());
                        $guestStars[] = $guestStar;
                    }
                    // Cast::upsert(collect($guestStars)->unique('credit_id', true)->toArray(), 'credit_id');
                    $c = GuestStar::wherein('credit_id', collect($guestStars)->pluck('credit_id')->unique())->pluck('id')->unique();
                    $modelepisode->guest_star()->sync($c);
                }

                if(isset($episode['translations'])){
                    $translations = collect($episode['translations'])->toArray();
                    Translation::upsert($translations, ['tv_id', 'season_id', 'episode_id', 'iso_3166_1']);
                }

                if(isset($episode['person_translations'])){
                    $translations = collect($episode['person_translations'])->toArray();
                    Translation::upsert($translations, ['tv_id', 'iso_639_1']);
                }

                $images = (new ClientImages($episode));
                Media::upsert($images->table(), 'file_path');

                foreach($images as $img){
                    $imagesArray[] = $img;
                }
            // }

        }

        foreach($imagesArray as $img){
            $this->imagesArray[] = $img;
        }

        return $this->imagesArray;

    }

    /**
     * All episode images.
     *
     * @return collection
     */
    public function images(){
        return $this->imagesArray;
    }

    public function toArray(){
        return collect($this)->toArray();
    }

}









