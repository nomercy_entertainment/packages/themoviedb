<?php

namespace Nomercy\Themoviedb\Http\Controllers\Database;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Nomercy\Themoviedb\Models\Cast;
use Nomercy\Themoviedb\Models\Crew;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Nomercy\Themoviedb\Models\Genre;
use Nomercy\Themoviedb\Models\Media;
use Nomercy\Themoviedb\Models\Movie;
use Nomercy\Themoviedb\Facades\Helper;

use Nomercy\Themoviedb\Facades\Logger;
use Nomercy\Themoviedb\Models\Creator;
use Nomercy\Themoviedb\Models\Similar;
use Nomercy\Themoviedb\Models\Translation;
use Nomercy\Themoviedb\Models\Certification;
use Nomercy\Themoviedb\Models\Recommendation;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientGenre;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientImages;
use Nomercy\Themoviedb\Http\Controllers\Client\ClientCertifications;

class DatabaseMovie extends Controller
{
    public $imagesArray;

    /**
     * Inserts all movie data to database.
     *
     * @return array
     * @return DatabaseFiller
     */
    public function build($movie, $forceUPdate = false){

        $this->movie = $movie;

        if(isset($movie['id']) &&  $movie['id'] != null){

            if(Genre::count() < 1){
                new ClientGenre();
            }

            if(Certification::count() < 1){
                new ClientCertifications();
            }
            $dbMovie = collect($movie)
                ->only(
                    "adult",
                    "backdrop",
                    "budget",
                    "duration",
                    "homepage",
                    "id",
                    "imdb_id",
                    "original_language",
                    "original_title",
                    "overview",
                    "popularity",
                    "poster",
                    "release_date",
                    "revenue",
                    "status",
                    "title",
                    "title_sort",
                    "trailer",
                    "video",
                    "vote_average",
                )
                ->toArray();

                if($dbMovie != []){
                    $now = Carbon::now();
                    // $diff = Carbon::createFromTimestamp(Movie::where('id', $movie['id'])->first()->updated_at ?? Carbon::now()->subYear(1)->toDateString() )->diffInDays($now);

                    // if($diff > config('themoviedb.not_update_before') || $forceUPdate){

                    Logger::info('Adding movie ' . $movie['id'] . ' Adding movie to the database');
                    Movie::upsert($dbMovie, 'id');
                    $modelMovie = Movie::find($movie['id']);

                    if(isset($movie['created_by'])){
                        $creators = [];
                        foreach($movie['created_by'] as $creator){
                            $creators[] = $creator;
                        }
                        Creator::upsert($creators, 'id');
                        $modelMovie->creator()->sync(collect($creators)->pluck('id'))->unique();
                    }

                    if(isset($movie['cast'])){
                        $casts = [];
                        foreach($movie['cast'] as $cast){
                            $casts[] = $cast;
                        }
                        Cast::upsert($casts, 'credit_id');
                        $c = Cast::wherein('credit_id', collect($casts)->pluck('credit_id'))->pluck('id')->unique();
                        $modelMovie->cast()->sync($c);
                    }

                    if(isset($movie['crew'])){
                        $crews = [];
                        foreach($movie['crew'] as $crew){
                            $crews[] = $crew;
                        }
                        Crew::upsert($crews, 'credit_id');
                        $c = Crew::wherein('credit_id', collect($crews)->pluck('credit_id'))->pluck('id')->unique();
                        $modelMovie->crew()->sync($c);
                    }


                    if(isset($movie['genres'])){
                        $modelGenres = Genre::whereIn('genre_id', $movie['genres'])->get()->pluck('id')->unique();
                        $modelMovie->genres()->sync($modelGenres);
                    }

                    if(isset($movie['translations'])){
                        $translations = collect($movie['translations'])->toArray();
                        Translation::upsert($translations, ['movie_id', 'iso_639_1']);
                    }

                    if(isset($movie['person_translations'])){
                        $translations = collect($movie['person_translations'])->toArray();
                        Translation::upsert($translations, ['movie_id', 'iso_639_1']);
                    }

                    if(isset($movie['videos'])){
                        $videos = collect($movie['videos'])->map(function($video) {
                            return collect($video)->merge([
                                'movie_id' => $this->movie['id']
                            ]);
                        })->toArray();
                        Media::upsert($videos, ['movie_id', 'iso_639_1']);
                    }

                    if(config('themoviedb.download_trailers')){
                        foreach ($movie['videos'] as $item) {
                            if ($item['type'] == 'Trailer') {
                                Logger::info('Adding movie ' . $movie['id'] . ' Downloading trailer ' . $item['key']);

                                $cmd = Helper::youtubeDl() . " -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --quiet --merge-output-format mp4 '" . $item['key'] . "' -o  '/var/www/html/storage/app/assets/trailer/movie/" . $movie['id'] . ".trailer.%(ext)s' --geo-bypass-country US --ffmpeg-location ". Helper::ffmpeg();
                                exec($cmd, $output);
                                Logger::info('Adding movie ' . $movie['id'] . collect($output)->merge($output)->except([3, 4, 5, 6]));
                            }

                            if (file_exists("/assets/trailer/" . $movie['id'] . ".trailer.mp4")) {
                                break;
                            }
                        }

                        foreach ($movie['videos'] as $item) {
                            if (!file_exists(storage_path('app/assets/trailer/movie/' . $movie['id'] . '.trailer.mp4'))) {
                                Logger::info('Adding movie ' . $movie['id'] . ' Downloading backup trailer ' . $item['key']);

                                $cmd = Helper::youtubeDl() . " -f 'besmovieideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --quiet --merge-output-format mp4 '" . $item['key'] . "' -o  '/var/www/html/storage/app/assets/trailer/movie/" . $movie['id'] . ".trailer.%(ext)s' --geo-bypass-country US --ffmpeg-location ". Helper::ffmpeg();
                                exec($cmd, $output);
                                Logger::info('Adding movie ' . $movie['id'] . collect($output)->merge($output)->except([3, 4, 5, 6]));
                            }
                        }
                    }

                    // $alternativeTitles = collect($movie)->only('alternative_titles')->first();
                    // $externalIds = collect($movie)->only('external_ids')->first();
                    // $externalLinks = collect($movie)->only('external_links')->first();
                    // $keywords = collect($movie)->only('keywords')->first();
                    // $networks = collect($movie)->only('networks')->first();
                    // $productionCompanies = collect($movie)->only('production_companies')->first();
                    // $contentRatings = collect($movie)->only('release_dates')->first();
                    // $reviews = collect($movie)->only('reviews')->first();
                    // $providers = collect($movie)->only('watch/providers')->first();

                    $m = new ClientImages($movie);
                    Media::upsert($m->table(), 'file_path');

                    if(config('themoviedb.download_images')){
                        Logger::info('Adding movie ' . $movie['id'] . ' Downloading show images');
                        Helper::download($m->ffmpeg(), 'movie');
                    }


                    if(isset($movie['recommendations'])){
                        foreach($movie['recommendations'] as $item){
                            if(Movie::where('id', $item['recommendation_movie_id'])->count() != 0){
                                Recommendation::updateOrCreate(['recommendation_movie_id' => $item['recommendation_movie_id']], $item);
                            }
                        }
                    }
                    if(isset($movie['similars'])){
                        foreach($movie['similars'] as $item){
                            Similar::updateOrCreate(['similar_movie_id' => $item['similar_movie_id']], $item);
                        }
                    }

                    // if(isset($movie['recommendations'])){
                    //     $list = collect($movie['recommendations'])->unique('movie_id')->toArray();
                    //     Recommendation::upsert($list, 'recommendation_movie_id');

                    // };

                    // if(isset($movie['similars'])){
                    //     $list = collect($movie['similars'])->unique('movie_id')->toArray();
                    //     Similar::upsert($list, 'similar_movie_id');
                    // };

                // }
                // else {
                //     Logger::info('Adding movie ' . $movie['id'] . ' is up to date.');
                // }
            }
            return $movie;
        }
    }

    public function toArray(){
        return collect($this)->toArray();
    }
}
