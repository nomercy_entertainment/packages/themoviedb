<?php

namespace Nomercy\Themoviedb\Controllers;

use Carbon\Carbon;
use App\Models\ApiUsage;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Nomercy\Themoviedb\Facades\Data;
use Nomercy\Themoviedb\Facades\Logger;

class ApiController extends Controller
{

    public $token, $id, $lang, $tv, $user;

    public function __construct($id = null, $season = null, $episode = null)
    {

        $this->middleware('api');

        $id = $this->id ?? $id;
        $season = $this->season ?? $season;
        $episode = $this->episode ?? $episode;

    }

    public function tv($id)
    {

        $list = Data::tv($id);

        if ($list == null) {
            Logger::notFound($id);
        };

        return response()->json([
            "status" => 'ok',
            "data" => $list
        ]);
    }

    public function season($id, $season)
    {

        $list = Data::season($id, $season);

        if ($list == null) {
            Logger::notFound($id);
        };

        return response()->json([
            "status" => 'ok',
            "data" => $list
        ]);
    }

    public function episode($id, $season, $episode)
    {

        $list = Data::episode($id, $season, $episode);

        if ($list == null) {
            Logger::notFound($id);
        };

        return response()->json([
            "status" => 'ok',
            "data" => $list
        ]);
    }

    public function movie($id)
    {
        $list = Data::movie($id);

        if ($list == null) {
            Logger::notFound($id);
        };

        return response()->json([
            "status" => 'ok',
            "data" => $list
        ]);
    }

    public function genres($id)
    {

        $list = Data::genres($id);

        if ($list == null) {
            Logger::notFound($id);
        };

        return response()->json([
            "status" => 'ok',
            "data" => $list
        ]);
    }

    public function ratings()
    {
        $list = Data::ratings();

        if ($list == null || $list->count() == 0) {
            Logger::notFound(null);
        };

        return response()->json([
            "status" => 'ok',
            "data" => $list
        ]);
    }

    public function tv_recommendations($id)
    {
        $list = Data::tv_recommendations($id);

        if ($list == null || $list->count() == 0) {
            Logger::notFound($id);
        };

        return response()->json([
            "status" => 'ok',
            "data" => $list
        ]);
    }

    public function movie_recommendations($id)
    {
        $list = Data::movie_recommendations($id);

        if ($list == null || $list->count() == 0) {
            Logger::notFound($id);
        };

        return response()->json([
            "status" => 'ok',
            "data" => $list
        ]);
    }

    public function certifications()
    {

        $list = Data::certifications();

        return response()->json([
            "status" => 'ok',
            "data" => $list
        ]);
    }


    public function loop_all_tv(Request $request, $items = null)
    {
        $data = [];

        if ($request->items != null || $items != null) {
            $items = $request->items ?? $items;

            if (!is_array($items)) {
                $items = explode(',', $items);
            }
            if (count($items) > 5) {
                return response()->json([
                    'status' => 'error',
                    'message' => __('themoviedb::error.tomannyitemsrequested'),
                ]);
            }
            foreach ($items as $i) {
                Data::tv($i);;
            }
        }

        return response()->json([
            "status" => 'ok',
        ]);
    }
}
