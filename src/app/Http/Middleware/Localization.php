<?php

namespace Nomercy\Themoviedb\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class Localization
{
    public function handle($request, Closure $next)
    {
        $headers = $request->headers->all();

        $languages = collect($headers["accept-language"] 
            ?? config('themoviedb.set_language') 
            ?? ['en'])
            ->first();
        $language = explode(',', explode(';', $languages)[0])[0];

        app()->setLocale(strtolower($language));
        Carbon::setLocale(strtolower($language));

        $country = collect($headers["accept-country"] 
            ?? config('themoviedb.set_country') 
            ?? request()->headers->all()['cf-ipcountry'][0]
            ?? ['US'])
            ->first();

        session(['country' => $country]);

        return $next($request);
    }
}
