<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Translation extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    public $table = 'translation';

// Relations

    public function episode()
    {
        return $this->belongsToMany(Episode::class);
    }
    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }
    public function season()
    {
        return $this->belongsToMany(Season::class);
    }
    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }

}
