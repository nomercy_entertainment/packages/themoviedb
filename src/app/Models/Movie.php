<?php

namespace Nomercy\Themoviedb\Models;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Nomercy\Themoviedb\Facades\Helper;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $table = 'movie';

    public function delete()
    {
        $this->translation()->delete();
        $this->media()->delete();
        $this->crew()->detach();
        $this->cast()->detach();
        $this->recommendations()->detach();

        return parent::delete();
    }

// Relations

    public function backdrops()
    {
        return $this->hasMany(Media::class)
            ->where('type', 'backdrop');
    }
    public function cast()
    {
        return $this->belongsToMany(Cast::class)->orderBy('order');
    }
    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }
    public function collection()
    {
        return $this->belongsToMany(Collection::class);
    }
    public function countries()
    {
        return $this->belongsToMany(Company::class);
    }
    public function crew()
    {
        return $this->belongsToMany(Crew::class);
    }
    public function genres()
    {
        return $this->belongsToMany(Genre::class)
            ->where('iso_639_1', app()->getLocale());
    }
    public function media()
    {
        return $this->hasMany(Media::class);
    }
    public function posters()
    {
        return $this->hasMany(Media::class)
            ->where('type', 'poster');
    }
    public function recommendations()
    {
        return $this->hasMany(Recommendation::class, 'recommendation_movie_id');
    }
    public function similars()
    {
        return $this->hasMany(Similar::class, 'similar_movie_id');
    }
    public function trailer()
    {
        return $this->hasMany(Media::class)
            ->where('type', 'Trailer');
    }
    public function translation()
    {
        return $this->hasMany(Translation::class)
            ->where('iso_639_1', app()->getLocale())
            ->where('iso_639_1', '!=', 'en');
    }
    public function videos()
    {
        return $this->hasMany(Media::class)
            ->where('key', '<>', null)
            ->where('type', '!=', 'Trailer');
    }


// Mutators

    public function getBackdropAttribute()
    {
        return config('themoviedb.download_images') && $this->attributes['backdrop'] != null
            ? asset('/assets/movie' . str_replace('.jpg', '.original.jpg', $this->attributes['backdrop']))
            : Helper::img_url($this->attributes['backdrop'], 'original');
    }
    public function getCreatedAtAttribute()
    {
        return Carbon::createFromDate($this->attributes['created_at'])
            ->format(__('themoviedb::time.format.date'));
    }
    public function getDirectorAttribute()
    {
        $director = [];
        foreach ($this->crew as $item) {
            if ($item->job == 'Director' || $item->job == 'Directing') {
                $director[] = $item->name;
            } else if (strpos($item->job, 'Director') == true) {
                $director[] = $item->name;
            }
        }
        $director = array_unique($director);
        return $director;
    }
    public function getDirectorsAttribute()
    {
        $director = [];
        foreach ($this->crew as $item) {
            if ($item->job == 'Director' || $item->job == 'Directing') {
                $director[] = $item;
            } else if (strpos($item->job, 'Director') == true) {
                $director[] = $item;
            }
        }
        $director = array_unique($director);
        return collect($director)->sortBy('order')->take(5);
    }
    public function getDurationAttribute()
    {
        $runtime = Helper::hoursandmins((int) $this->attributes['duration']);
        return $runtime;
    }
    public function getLeadActorsAttribute()
    {
        $actors = $this->cast->sortBy('order')->take(5);
        return $actors;
    }
    public function getOriginAttribute()
    {
        return env('APP_URL');
    }
    public function getOverviewAttribute()
    {
        $trans = $this->translation()->pluck('overview')->first();
        return $trans != null && $trans != ''
            ? $trans
            : $this->attributes['overview'];

    }
    public function getPosterAttribute()
    {
        return config('themoviedb.download_images') && $this->attributes['poster'] != null
            ? asset('/assets/movie' . $this->attributes['poster'])
            : Helper::img_url($this->attributes['backdrop'], 'original');
    }
    // public function getRecommendationsAttribute(){

    //     $ids = Recommendation::where('movie_id', $this->id)
    //         ->get()
    //         ->pluck('recommendation_id')
    //         ->toArray()
    //         ;

    //     return Movie::whereIn('id', $ids)
    //         ->select([
    //             'id',
    //             'title_sort',
    //             'overview',
    //             'duration',
    //             'backdrop',
    //             'poster'
    //         ])
    //         ->get();
    // }
    // public function getReleaseDateAttribute()
    // {
    //     return Carbon::parse($this->attributes['release_date'])->format('Y');
    // }
    public function getTitleAttribute()
    {
        $trans = $this->translation()->pluck('title')->first();
        return $trans != null && $trans != ''
            ? $trans
            : $this->attributes['title'];
    }
    public function getTrailerAttribute()
    {
        return config('themoviedb.download_images') && file_exists(storage_path('app/asset/trailers/'. $this->id . '.trailer.mp4'))
            ? asset('assets/trailer/'. $this->id . '.trailer.mp4')
            : $this->trailer()->pluck('key')->first()
            . '?ecver=1&iv_load_policy=3&rel=0&showinfo=0&yt:stretch=16:9&autohide=1&color=white&width=560&width=560&controls=0&modestbranding=1';
    }
    // public function getUpdatedAtAttribute()
    // {
    //     return Carbon::createFromDate($this->attributes['updated_at'])
    //         ->format(__('themoviedb::time.format.date'));
    // }

}
