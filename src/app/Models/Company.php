<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot'
    ];
    protected $primaryKey = 'id';
    public $table = 'company';
    public $timestamps = false;

// Relations

    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }
    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }
}
