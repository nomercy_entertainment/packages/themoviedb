<?php

namespace Nomercy\Themoviedb\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Nomercy\Themoviedb\Facades\Helper;
use Nomercy\Themoviedb\Models\Similar;
use Illuminate\Database\Eloquent\Model;

class Tv extends Model
{
    protected $table = 'tv';
    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot'
    ];
    protected $guarded = [];
    public $primaryKey = 'id';

    public function delete()
    {
        $this->seasons()->delete();
        $this->translation()->delete();
        $this->guest_star()->delete();
        $this->media()->delete();
        $this->creator()->delete();
        $this->crew()->detach();
        $this->cast()->detach();
        $this->recommendations()->detach();

        return parent::delete();
    }

    public function format()
    {
        return [
            'backdrop' =>  $this->backdrop()->first()->file_path ?? $this->backdrop,
            'backdrops' =>  $this->backdrops,
            'cast' =>  $this->cast,
            'creator' =>  $this->creator,
            'crew' =>  $this->crew,
            'directors' =>  $this->directors,
            'duration' =>  Helper::hoursandmins((int) $this->duration),
            'first_air_date' => Carbon::createFromTimestamp($this->first_air_date)
                                    ->format(__('themoviedb::time.format.date')),
            'year' => Carbon::createFromTimestamp($this->first_air_date)
                                    ->format('Y'),
            'genres' =>  $this->genres,
            'have_episodes' =>  $this->have_episodes,
            'homepage' =>  $this->homepage,
            'id'=>  $this->id,
            'in_production'=>  $this->in_production,
            'languages' =>  $this->languages,
            'last_air_date' =>  Carbon::createFromTimestamp($this->last_air_date)
                                    ->format(__('themoviedb::time.format.date')),
            'last_episode_to_air'=>  $this->last_episode_to_air,
            'lead_actors' =>  $this->cast->sortBy('order')->take(10),
            'media' =>  $this->media,
            'next_episode_to_air'=>  $this->next_episode_to_air,
            'number_of_episodes'=>  $this->number_of_episodes,
            'number_of_seasons'=>  $this->number_of_seasons,
            'origin' =>  env('APP_URL'),
            'origin_country' =>  $this->origin_country,
            'original_language' =>  $this->original_language,
            'overview' =>  Helper::translatable($this, 'overview'),
            'popularity'=>  $this->popularity,
            'poster' =>  $this->poster()->first()->file_path ?? $this->poster,
            'posters' =>  $this->posters,
            'recommendations' =>  $this->recommendations,
            'seasons' =>  $this->seasons,
            'status' =>  __('themoviedb::api.' . $this->status),
            'subtitles' =>  $this->subtitles,
            'title' =>  Helper::translatable($this, 'title'),
            'title_sort' =>  Helper::translatable($this, 'title'),
            'trailer' =>  $this->trailer,
            'videos' =>  $this->videos,
            'vote_average'=>  $this->vote_average,
        ];
    }

// Relations

    public function backdrops()
    {
        return $this->hasMany(Media::class)
            ->where('type', 'backdrop');
    }

    public function backdrop()
    {
        return $this->hasMany(Media::class)
            ->where('type', 'backdrop')
            ->whereIn('iso_639_1', [ app()->getLocale(), 'en']);
    }
    public function cast()
    {
        return $this->belongsToMany(Cast::class)
            ->orderBy('order');
    }
    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }
    public function creator()
    {
        return $this->belongsToMany(Creator::class);
    }
    public function crew()
    {
        return $this->belongsToMany(Crew::class);
    }
    public function directors()
    {
        return $this->belongsToMany(Crew::class)
            ->where('known_for_department', 'Director')
            ->orWhere('known_for_department', 'Directing');
    }
    public function genres()
    {
        return $this->belongsToMany(Genre::class)
            ->where('iso_639_1', app()->getLocale());
    }
    public function lead_actors()
    {
        return $this->belongsToMany(Cast::class)
            ->orderBy('order')->take(10);
    }
    public function media()
    {
        return $this->hasMany(Media::class);
    }
    public function networks()
    {
        return $this->belongsToMany(Network::class);
    }
    public function posters()
    {
        return $this->hasMany(Media::class)
            ->where('type', 'poster');
    }
    public function poster()
    {
        return $this->hasMany(Media::class)
            ->where('type', 'poster')
            ->whereIn('iso_639_1', [ app()->getLocale(), 'en']);
    }
    public function recommendations()
    {
        return $this->hasMany(Recommendation::class, 'tv_id');
    }
    public function similars()
    {
        return $this->hasMany(Similar::class, 'tv_id');
    }
    public function seasons()
    {
        return $this->hasMany(Season::class)
            ->orderBy('season_number');
    }
    public function trailer()
    {
        return $this->hasMany(Media::class)
            ->where('type', 'Trailer');
    }
    public function translation()
    {
        return $this->hasMany(Translation::class)
            ->where('iso_639_1', app()->getLocale())
            ->where('iso_639_1', '!=', 'en')
            ->whereNotNull('tv_id')
            ->whereNull('season_id')
            ->whereNull('episode_id')
            ;
    }
    public function videos()
    {
        return $this->hasMany(Media::class)
            ->where('key', '<>', null)
            ->where('type', '!=', 'Trailer');
    }


    public function getBackdropAttribute()
    {
        return config('themoviedb.download_images') && $this->attributes['backdrop'] != null
            ? asset('/assets/movie' . str_replace('.jpg', '.original.jpg', $this->attributes['backdrop']))
            : Helper::img_url($this->attributes['backdrop'], 'original');
    }
    public function getPosterAttribute()
    {
        return config('themoviedb.download_images') && $this->attributes['poster'] != null
            ? asset('/assets/movie' . $this->attributes['poster'])
            : Helper::img_url($this->attributes['backdrop'], 'original');
    }
    public function getTrailerAttribute()
    {
        return config('themoviedb.download_images') && file_exists(storage_path('app/asset/trailers/'. $this->id . '.trailer.mp4'))
            ? asset('assets/trailer/'. $this->id . '.trailer.mp4')
            : $this->trailer()->pluck('key')->first()
            . '?ecver=1&iv_load_policy=3&rel=0&showinfo=0&yt:stretch=16:9&autohide=1&color=white&width=560&width=560&controls=0&modestbranding=1';
    }

}
