<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Lang;
use Illuminate\Database\Eloquent\Model;
use Nomercy\Themoviedb\Facades\Helper;

class Season extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot'
    ];
    protected $primaryKey = 'id';
    public $table = 'season';
    protected $with = [
        // 'episodes'
    ];

    public function delete()
    {
        $this->episodes()->delete();
        $this->translation()->delete();
        $this->guest_star()->delete();
        $this->media()->delete();
        $this->crew()->detach();
        $this->cast()->detach();

        return parent::delete();
    }


// Relations

    public function cast()
    {
        return $this->belongsToMany(Cast::class)
            ->orderBy('order');
    }
    public function crew()
    {
        return $this->belongsToMany(Crew::class);
    }
    public function episodes()
    {
        return $this->hasMany(Episode::class)
            ->orderBy('episode_number');
    }
    public function media()
    {
        return $this->hasMany(Media::class);
    }
    public function posters()
    {
        return $this->hasMany(Media::class)->where('type', 'poster')
            ->whereNull('tv_id')->whereNull('episode_id');
    }
    public function translation()
    {
        return $this->hasMany(Translation::class)
            ->where('iso_639_1', app()->getLocale())
            ->where('iso_639_1', '!=', 'en');
    }
    public function tv()
    {
        return $this->belongsTo(Tv::class);
    }


// Mutators

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromDate($this->attributes['created_at'])
            ->format(__('themoviedb::time.format.date'));
    }
    public function getDirectorsAttribute()
    {
        $tv = Season::where('tv_id', $this->id)
            ->with('crew')
            ->get();

        $crew = [];
        foreach ($tv as $episode) {
            foreach ($episode->crew as $item) {
                if ($item->job == 'Director') {
                    $crew[] = $item->name;
                }
            }
        }
        $count = array_count_values($crew);
        arsort($count);

        foreach ($count as $item => $amount) {
            if ($amount > 10) {
                $director[] = $item;
            }
        }

        return str_replace(',', ', ', implode(",", $director));
    }
    public function getFirstAirDateAttribute()
    {
        return Carbon::createFromDate($this->attributes['first_air_date'])
            ->format(__('themoviedb::time.format.date'));
    }
    public function getLastAirDateAttribute()
    {
        return Carbon::createFromDate($this->attributes['last_air_date'])
            ->format(__('themoviedb::time.format.date'));
    }
    public function getOverviewAttribute()
    {
        $trans = $this->translation()->pluck('overview')->first();
        return $trans
            ?? $this->attributes['overview']
            ?? null;
    }
    public function getPosterAttribute()
    {
        return $this->attributes['poster'] != null && config('themoviedb.download_images')
            ? asset('/assets/tv' . $this->attributes['poster'])
            : Helper::img_url($this->attributes['poster'], 'original');
    }
    public function getSeasonNumberAttribute()
    {
        return sprintf("%02d", $this->attributes['season_number']);
    }
    public function getTitleAttribute()
    {
        $trans = $this->translation()->pluck('title')->first();
        return $trans != null && $trans != ''
            ? $trans
            : $this->attributes['title'];
    }
    public function getTypeAttribute()
    {
        return 'tv';
    }
    public function getUpdatedAtAttribute()
    {
        return Carbon::createFromDate($this->attributes['updated_at'])
            ->format(__('themoviedb::time.format.date'));
    }


}
