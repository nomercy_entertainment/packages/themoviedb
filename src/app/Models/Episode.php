<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Lang;
use Illuminate\Database\Eloquent\Model;
use Nomercy\Themoviedb\Facades\Helper;

class Episode extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
        'genre_id',
        'pivot'
    ];
    protected $orderBy = [
        'season',
        'episode',
    ];
    protected $orderDirection = 'ASC';
    protected $primaryKey = 'id';
    public $table = 'episode';

    public function delete()
    {
        $this->media()->delete();
        $this->translation()->delete();
        $this->guest_star()->delete();
        $this->media()->delete();
        $this->crew()->detach();
        $this->cast()->detach();
        return parent::delete();
    }


// Relations

    public function cast()
    {
        return $this->belongsToMany(Cast::class)
            ->orderBy('order');
    }
    public function creators()
    {
        return $this->belongsToMany(Creator::class);
    }
    public function crew()
    {
        return $this->belongsToMany(Crew::class);
    }
    public function guest_star()
    {
        return $this->belongsToMany(GuestStar::class)
            ->orderBy('order');
    }
    public function season()
    {
        return $this->belongsTo(Season::class)
            ->orderBy('season_id')
            ->orderBy('episode_id');;
    }
    public function translation()
    {
        return $this->hasMany(Translation::class)
            ->where('iso_639_1', app()->getLocale())
            ->where('iso_639_1', '!=', 'en');
    }
    public function tv()
    {
        return $this->belongsTo(Tv::class);
    }


// Mutators

    public function getAirDateAttribute()
    {
        return Carbon::createFromDate($this->attributes['air_date'])
            ->format(__('themoviedb::time.format.date'));
    }
    public function getCreatedAtAttribute()
    {
        return Carbon::createFromDate($this->attributes['created_at'])
            ->format(__('themoviedb::time.format.date'));
    }
    public function getEpisodeNumberAttribute()
    {
        return sprintf("%02d", $this->attributes['episode_number']);
    }
    public function getOverviewAttribute()
    {
        $trans = $this->translation()->pluck('overview')->first();
        return $trans
            ?? $this->attributes['overview']
            ?? null;
    }
    public function getSeasonNumberAttribute()
    {
        return sprintf("%02d", $this->attributes['season_number']);
    }
    public function getStillAttribute()
    {
        return $this->attributes['still'] != null && config('themoviedb.download_images')
            ? asset('/assets/tv' . $this->attributes['still'])
            : Helper::img_url($this->attributes['still'], 'original');
    }
    public function getTitleAttribute()
    {
        $trans = $this->translation()->pluck('title')->first();
        return $trans != null && $trans != ''
            ? $trans
            : $this->attributes['title'];
    }
    public function getUpdatedAtAttribute()
    {
        return Carbon::createFromDate($this->attributes['updated_at'])
            ->format(__('themoviedb::time.format.date'));
    }

}
