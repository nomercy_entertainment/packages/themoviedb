<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Database\Eloquent\Model;
use Nomercy\Themoviedb\Facades\Helper;

class Crew extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
        'genre_id',
        'pivot'
    ];
    protected $primaryKey = 'id';
    public $table = 'crew';
    public $timestamps = false;


// Relations

    public function episode()
    {
        return $this->belongsToMany(Episode::class);
    }
    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }
    public function season()
    {
        return $this->belongsToMany(Season::class);
    }
    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }

 //Mutators

    public function getProfilePathAttribute()
    {
        return $this->attributes['profile_path'] != null && config('themoviedb.download_images')
            ? asset('/assets/profile' . str_replace('.jpg', '.original.jpg', $this->attributes['profile_path']))
            : ($this->attributes['profile_path'] != null ? Helper::img_url($this->attributes['profile_path'], 'w185') : '');
    }

}
