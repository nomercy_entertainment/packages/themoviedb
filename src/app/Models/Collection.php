<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot'
    ];
    protected $primaryKey = 'id';
    protected $table = 'collection';
    public $timestamps = false;

// Relations

    public function movies()
    {
        return $this->belongsToMany(Movie::class);
    }
}
