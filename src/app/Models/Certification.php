<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Support\Facades\Lang;
use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot'
    ];
    protected $primaryKey = 'id';
    public $table = 'certification';
    public $timestamps = false;

// Relations

    public function movie()
    {
        return $this->belongsToMany(Movie::class)
            ->where('iso_3166_1', session('country'));
    }
    public function tv()
    {
        return $this->belongsToMany(Tv::class)
            ->where('iso_3166_1', session('country'));
    }

// Mutators

    public function getMeaningAttribute()
    {
        return Lang::has('themoviedb::certification.' . $this->attributes['iso_3166_1'] . '.' . $this->attributes['rating'])
            ? __('themoviedb::certification.' . $this->attributes['iso_3166_1'] . '.' . $this->attributes['rating'])
            : $this->attributes['meaning'];
    }
}
