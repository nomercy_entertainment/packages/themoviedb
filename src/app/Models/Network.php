<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $table = 'network';

// Relations

    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }
    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }
}
