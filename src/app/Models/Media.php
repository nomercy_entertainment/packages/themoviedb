<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Support\Carbon;
use Nomercy\Themoviedb\Facades\Helper;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot'
    ];
    protected $guarded = [];
    public $primaryKey = 'id';

// Relations

    public function episode()
    {
        return $this->belongsToMany(Episode::class);
    }
    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }
    public function season()
    {
        return $this->belongsToMany(Season::class);
    }
    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }

// Mutators

    public function getFilePathAttribute()
    {
        return $this->attributes['file_path'] != null && config('themoviedb.download_images')
            ? asset('/assets/tv' . $this->attributes['file_path'])
            : ($this->attributes['file_path'] != null ? Helper::img_url($this->attributes['file_path'], 'original') : null);
    }

    public function getKeyAttribute()
    {
        return $this->attributes['key'] != null
            ? 'https://www.youtube.com/embed/' . $this->attributes['key']
            : null;
    }

}
