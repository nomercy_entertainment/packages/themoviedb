<?php

namespace Nomercy\Themoviedb\Models;

use Nomercy\Themoviedb\Facades\Helper;
use Illuminate\Database\Eloquent\Model;

class Similar extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'id',
        'tv_id',
        'movie_id',
        'episodes',
        'tv_id',
        'updated_at',
        'updated_at',
        'pivot'
    ];
    protected $primaryKey = 'id';
    public $table = 'similar';
    public $timestamps = false;

 // Relations

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }
    public function tv()
    {
        return $this->belongsTo(Tv::class);
    }

 // Mutators

    public function getBackdropAttribute()
    {
        return $this->attributes['backdrop'] != null && config('themoviedb.download_images')
            ? asset('/assets/tv' . str_replace('.jpg', '.original.jpg', $this->attributes['backdrop']))
            : Helper::img_url($this->attributes['backdrop'], 'original');
    }

    public function getPosterAttribute()
    {
        return config('themoviedb.download_images') && $this->attributes['poster'] != null
            ? asset('/assets/movie' . $this->attributes['poster'])
            : Helper::img_url($this->attributes['poster'], 'original');
    }
}
