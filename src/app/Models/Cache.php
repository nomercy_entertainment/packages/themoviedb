<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Database\Eloquent\Model;

class Cache extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'key';
    public $table = 'cache';

    public function getKeyAttribute()
    {
        return $this->attributes['key'];
    }

}
