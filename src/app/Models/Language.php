<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $table = 'language';

}
