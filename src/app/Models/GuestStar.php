<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Database\Eloquent\Model;
use Nomercy\Themoviedb\Facades\Helper;

class GuestStar extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot'
    ];
    protected $primaryKey = 'id';
    public $table = 'guest_star';
    public $timestamps = false;

// Relations

    public function episode()
    {
        return $this->belongsToMany(Episode::class);
    }


 //Mutators

    public function getProfilePathAttribute()
    {
        return $this->attributes['profile_path'] != null && config('themoviedb.download_images')
            ? asset('/assets/profile' . str_replace('.jpg', '.original.jpg', $this->attributes['profile_path']))
            : Helper::img_url($this->attributes['profile_path'], 'w185');
    }

}
