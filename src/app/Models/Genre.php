<?php

namespace Nomercy\Themoviedb\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
        'genre_id',
        'pivot'
    ];
    protected $primaryKey = 'id';
    public $table = 'genre';
    public $timestamps = false;

// Relations

    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }
    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }

// Mutators

    public function getGenresAttribute()
    {
        return implode(', ', $this->attributes['genres']->pluck('name'));
    }

}
