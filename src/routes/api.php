<?php

use App\Http\Middleware\ApiToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Nomercy\Themoviedb\Controllers\ApiController;
use Nomercy\Themoviedb\Controllers\ThemoviedbController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => [
        ApiToken::class,
        'token',
        Nomercy\Themoviedb\Http\Middleware\Localization::class,
    ],

], function () {


    Route::group([
        'prefix' => 'database',
    ], function () {

        Route::get('build_tv/{id}',                 [ThemoviedbController::class, 'build_tv']);
        Route::get('store_tv/{id}',                 [ThemoviedbController::class, 'store_tv']);

        Route::get('tv/{id}',                       [ThemoviedbController::class, 'tv']);
        Route::get('tv/{id}/{season}',              [ThemoviedbController::class, 'season']);
        Route::get('tv/{id}/{season}/{episode}',    [ThemoviedbController::class, 'episode']);

        Route::get('build_movie/{id}',              [ThemoviedbController::class, 'build_movie']);
        Route::get('store_movie/{id}',              [ThemoviedbController::class, 'store_movie']);

        Route::get('movie/{id}',                    [ThemoviedbController::class, 'movie']);

        Route::get('genres',                        [ThemoviedbController::class, 'genres']);
        Route::get('ratings',                       [ThemoviedbController::class, 'ratings']);
        Route::get('certifications',                [ThemoviedbController::class, 'certifications']);


        Route::get('batch',                         [ThemoviedbController::class, 'loop_all_tv']);
    });

    Route::group([], function () {

        Route::get('tv/{id}',                       [ApiController::class, 'tv']);
        Route::get('tv/{id}/recommendations',       [ApiController::class, 'tv_recommendations']);
        Route::get('tv/{id}/{season}',              [ApiController::class, 'season']);
        Route::get('tv/{id}/{season}/{episode}',    [ApiController::class, 'episode']);

        Route::get('movie/{id}',                    [ApiController::class, 'movie']);
        Route::get('movie/{id}/recommendations',    [ApiController::class, 'movie_recommendations']);



        Route::get('genres',                        [ApiController::class, 'genres']);
        Route::get('ratings',                       [ApiController::class, 'ratings']);
        Route::get('certifications',                [ApiController::class, 'certifications']);


        Route::get('batch',                         [ApiController::class, 'loop_all_tv']);

    });
});
