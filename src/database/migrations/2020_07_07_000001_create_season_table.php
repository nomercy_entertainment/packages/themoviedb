<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Nomercy\Themoviedb\Models\Tv;

class CreateSeasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('season', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->id();
            $table->integer("season_number");
            $table->text("title",500)->nullable();
            $table->mediumText("overview")->nullable();
            $table->text("poster")->nullable();
            $table->date("air_date")->nullable();
            $table->text("tvdb_id")->nullable();
            $table->timestamps();

            $table->unsignedBigInteger('tv_id')->nullable();
            $table->foreign('tv_id')->references('id')->on('tv')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('season');
    }
}
