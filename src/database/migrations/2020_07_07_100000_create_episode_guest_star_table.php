<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodeGuestStarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episode_guest_star', function (Blueprint $table) {
            $table->unsignedBigInteger('guest_star_id');
            $table->foreign('guest_star_id')->references('id')->on('guest_star')->onDelete('cascade');

            $table->unsignedBigInteger('episode_id')->index();
            $table->foreign('episode_id')->references('id')->on('episode')->onDelete('cascade');

            $table->unique(['guest_star_id', 'episode_id']);
            $table->primary(['guest_star_id', 'episode_id']);
            $table->index(['guest_star_id', 'episode_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_star_episode');
    }
}
