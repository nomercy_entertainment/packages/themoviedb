<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Nomercy\Themoviedb\Models\Season;

class CreateEpisodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episode', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->id();
            $table->integer('season_number');
            $table->integer('episode_number');
            $table->text('title',500)->nullable();
            $table->mediumText('overview')->nullable();
            $table->date('air_date')->nullable();
            $table->text('still')->nullable();
            $table->text('imdb_id')->nullable();
            $table->integer('tvdb_id')->nullable();
            $table->text('production_code')->nullable();
            $table->float('vote_average')->nullable();
            $table->integer('vote_count')->nullable();
            $table->timestamps();

            $table->foreignId('tv_id')->references('id')->on('tv')->onDelete('cascade');
            $table->unsignedBigInteger('season_id');
            // $table->foreign('season_id')->references('id')->on('season')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episode');
    }
}
