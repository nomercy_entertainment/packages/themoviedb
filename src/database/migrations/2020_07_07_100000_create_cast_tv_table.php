<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCastTvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cast_tv', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->unsignedBigInteger('cast_id');
            $table->foreign('cast_id')->references('id')->on('cast')->onDelete('cascade');

            $table->unsignedBigInteger('tv_id')->index();
            $table->foreign('tv_id')->references('id')->on('tv')->onDelete('cascade');

            $table->unique(['cast_id', 'tv_id']);
            $table->primary(['cast_id', 'tv_id']);
            $table->index(['cast_id', 'tv_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cast_tv');
    }
}
