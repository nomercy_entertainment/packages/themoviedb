<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreatorTvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creator_tv', function (Blueprint $table) {
            $table->unsignedBigInteger('creator_id');
            $table->foreign('creator_id')->references('id')->on('crew')->onDelete('cascade');

            $table->unsignedBigInteger('tv_id')->index();
            $table->foreign('tv_id')->references('id')->on('tv')->onDelete('cascade');

            $table->unique(['creator_id', 'tv_id']);
            $table->primary(['creator_id', 'tv_id']);
            $table->index(['creator_id', 'tv_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creator_tv');
    }
}
