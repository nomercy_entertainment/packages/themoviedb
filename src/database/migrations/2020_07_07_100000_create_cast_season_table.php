<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCastSeasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cast_season', function (Blueprint $table) {
            $table->bigInteger('cast_id')->unsigned();
            $table->foreign('cast_id')->references('id')->on('cast')->onDelete('cascade');

            $table->bigInteger('season_id')->unsigned()->index();
            $table->foreign('season_id')->references('id')->on('season')->onDelete('cascade');

            $table->unique(['cast_id', 'season_id']);
            $table->primary(['cast_id', 'season_id']);
            $table->index(['cast_id', 'season_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cast_season');
    }
}
