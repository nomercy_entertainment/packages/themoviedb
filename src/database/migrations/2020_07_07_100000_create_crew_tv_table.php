<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrewTvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crew_tv', function (Blueprint $table) {
            $table->unsignedBigInteger('crew_id');
            $table->foreign('crew_id')->references('id')->on('crew')->onDelete('cascade');

            $table->unsignedBigInteger('tv_id')->index();
            $table->foreign('tv_id')->references('id')->on('tv')->onDelete('cascade');

            $table->unique(['crew_id', 'tv_id']);
            $table->primary(['crew_id', 'tv_id']);
            $table->index(['crew_id', 'tv_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crew_tv');
    }
}
