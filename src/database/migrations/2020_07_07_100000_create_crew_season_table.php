<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrewSeasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crew_season', function (Blueprint $table) {
            $table->unsignedBigInteger('crew_id');
            $table->foreign('crew_id')->references('id')->on('crew')->onDelete('cascade');

            $table->unsignedBigInteger('season_id')->index();
            $table->foreign('season_id')->references('id')->on('season')->onDelete('cascade');

            $table->unique(['crew_id', 'season_id']);
            $table->primary(['crew_id', 'season_id']);
            $table->index(['crew_id', 'season_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crew_season');
    }
}
