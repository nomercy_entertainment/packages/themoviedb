<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecommendationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendation', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->id();
            $table->string('title');
            $table->string('title_sort');
            $table->mediumText('overview')->nullable();
            $table->string('backdrop')->nullable();
            $table->string('poster')->nullable();
            $table->float('vote_average')->nullable();

            $table->unsignedBigInteger('movie_id')->nullable()->index();
            $table->foreign('movie_id')->references('id')->on('movie')->onDelete('cascade');

            $table->unsignedBigInteger('recommendation_movie_id')->nullable()->index();
            // $table->foreign('recommendation_movie_id')->references('id')->on('movie')->onDelete('cascade');

            $table->unsignedBigInteger('tv_id')->nullable()->index();
            $table->foreign('tv_id')->references('id')->on('tv')->onDelete('cascade');

            $table->unsignedBigInteger('recommendation_tv_id')->nullable()->index();
            // $table->foreign('recommendation_tv_id')->references('id')->on('tv')->onDelete('cascade');

            $table->unique(['movie_id','recommendation_movie_id','tv_id','recommendation_tv_id'], 'unique');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendation');
    }
}
