<?php

use Nomercy\Themoviedb\Models\Tv;
use Nomercy\Themoviedb\Models\Season;
use Illuminate\Support\Facades\Schema;
use Nomercy\Themoviedb\Models\Episode;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crew', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->id();
            $table->string("name")->nullable();
            $table->integer("profile_id")->nullable();
            $table->integer("adult")->default(0);
            $table->mediumText("department")->nullable();
            $table->mediumText("job")->nullable();
            $table->string("credit_id")->nullable()->unique();
            $table->integer("gender")->nullable();
            $table->string("popularity")->nullable();
            $table->string("original_name")->nullable();
            $table->string("known_for_department")->nullable();
            $table->string("profile_path")->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crew');
    }
}
