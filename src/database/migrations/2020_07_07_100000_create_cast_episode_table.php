<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCastEpisodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cast_episode', function (Blueprint $table) {
            $table->bigInteger('cast_id')->unsigned();
            $table->foreign('cast_id')->references('id')->on('cast')->onDelete('cascade');

            $table->bigInteger('episode_id')->unsigned()->index();
            $table->foreign('episode_id')->references('id')->on('episode')->onDelete('cascade');

            $table->unique(['cast_id', 'episode_id']);
            $table->primary(['cast_id', 'episode_id']);
            $table->index(['cast_id', 'episode_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cast_episode');
    }
}
