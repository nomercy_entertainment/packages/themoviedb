<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->id();
            $table->string('title',500);
            $table->string('title_sort',500);
            $table->mediumText('overview')->nullable();
            $table->integer('duration')->nullable();
            $table->string('backdrop')->nullable();
            $table->string('poster')->nullable();
            $table->string('trailer')->nullable();
            $table->date('first_air_date')->nullable();
            $table->date('last_air_date')->nullable();
            $table->integer('number_of_episodes')->default(0)->nullable();
            $table->integer('number_of_seasons')->default(0)->nullable();

            $table->mediumText('homepage')->nullable();
            $table->boolean('in_production')->default(false);
            $table->integer('last_episode_to_air')->nullable();
            $table->integer('next_episode_to_air')->nullable();
            $table->string('origin_country')->nullable();
            $table->string('original_language')->nullable();
            $table->float('popularity')->nullable();
            $table->string('status')->nullable();
            $table->float('vote_average')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tv');
    }
}
