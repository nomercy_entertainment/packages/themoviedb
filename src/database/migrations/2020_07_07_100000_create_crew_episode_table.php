<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrewEpisodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crew_episode', function (Blueprint $table) {
            $table->unsignedBigInteger('crew_id');
            $table->foreign('crew_id')->references('id')->on('crew')->onDelete('cascade');

            $table->unsignedBigInteger('episode_id')->index();
            $table->foreign('episode_id')->references('id')->on('episode')->onDelete('cascade');

            $table->unique(['episode_id', 'crew_id']);
            $table->primary(['episode_id', 'crew_id']);
            $table->index(['episode_id', 'crew_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crew_episode');
    }
}
