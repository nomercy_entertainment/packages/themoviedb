<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->id();
            $table->string('title',500)->nullable();
            $table->string('adult')->nullable();
            $table->string('backdrop')->nullable();
            $table->string('budget')->nullable();
            $table->string('duration')->nullable();
            $table->mediumText('homepage')->nullable();
            $table->string('imdb_id')->nullable();
            $table->string('original_language')->nullable();
            $table->string('original_title',500)->nullable();
            $table->mediumText('overview')->nullable();
            $table->float('popularity')->nullable();
            $table->string('poster')->nullable();
            $table->string('release_date')->nullable();
            $table->string('revenue')->nullable();
            $table->string('status')->nullable();
            $table->string('title_sort',500)->nullable();
            $table->string('trailer')->nullable();
            $table->string('video')->nullable();
            $table->float('vote_average')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie');
    }
}
