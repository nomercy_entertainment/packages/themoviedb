<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->id();
            $table->string('type');
            $table->string('iso_639_1')->nullable();
            $table->string('iso_3166_1')->nullable();
            $table->string('file_path')->unique()->nullable();
            $table->string('key')->unique()->nullable();
            $table->string('name')->nullable();
            $table->string('site')->nullable();
            $table->string('size')->nullable();
            $table->string('aspect_ratio')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->integer("vote_count")->nullable();
            $table->string("vote_average")->nullable();
            $table->timestamps();

            $table->unsignedBigInteger('tv_id')->nullable()->index();
            $table->foreign('tv_id')->references('id')->on('tv')->onDelete('cascade');

            $table->unsignedBigInteger('season_id')->nullable()->index();
            // $table->foreign('season_id')->references('id')->on('season')->onDelete('cascade');

            $table->unsignedBigInteger('episode_id')->nullable()->index();
            // $table->foreign('episode_id')->references('id')->on('episode')->onDelete('cascade');

            $table->unsignedBigInteger('movie_id')->nullable()->index();
            $table->foreign('movie_id')->references('id')->on('movie')->onDelete('cascade');

            $table->unique(['movie_id','tv_id','season_id','episode_id', 'file_path', 'key']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
