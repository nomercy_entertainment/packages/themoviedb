<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->id();
            $table->string("iso_3166_1");
            $table->string("iso_639_1");
            $table->string("name")->nullable();
            $table->string("homepage")->nullable();
            $table->string("english_name")->nullable();
            $table->mediumText("title")->nullable();
            $table->mediumText("overview")->nullable();
            $table->mediumText("biography")->nullable();

            $table->unsignedBigInteger('person_id')->nullable()->index();
            $table->foreign('person_id', 'foreign_person_id_crew')->references('id')->on('crew')->onDelete('cascade');
            $table->foreign('person_id', 'foreign_person_id_cast')->references('id')->on('cast')->onDelete('cascade');

            $table->unsignedBigInteger('movie_id')->nullable()->index();
            $table->foreign('movie_id')->references('id')->on('movie')->onDelete('cascade');

            $table->unsignedBigInteger('tv_id')->nullable()->index();
            $table->foreign('tv_id')->references('id')->on('tv')->onDelete('cascade');

            $table->unsignedBigInteger('season_id')->nullable()->index();
            // $table->foreign('season_id')->references('id')->on('season')->onDelete('cascade');

            $table->unsignedBigInteger('episode_id')->nullable()->index();
            // $table->foreign('episode_id')->references('id')->on('episode')->onDelete('cascade');


            $table->unique(['movie_id','tv_id', 'season_id', 'episode_id', 'iso_3166_1'], 'unique_translation');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translation');
    }
}
