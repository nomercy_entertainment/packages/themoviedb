<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrewMovieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crew_movie', function (Blueprint $table) {
            $table->unsignedBigInteger('crew_id');
            $table->foreign('crew_id')->references('id')->on('crew')->onDelete('cascade');

            $table->unsignedBigInteger('movie_id')->index();
            $table->foreign('movie_id')->references('id')->on('movie')->onDelete('cascade');

            $table->unique(['crew_id', 'movie_id']);
            $table->primary(['crew_id', 'movie_id']);
            $table->index(['crew_id', 'movie_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crew_movie');
    }
}
