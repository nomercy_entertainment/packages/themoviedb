<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenreTvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genre_tv', function (Blueprint $table) {
            $table->unsignedBigInteger('genre_id');
            $table->foreign('genre_id')->references('id')->on('genre')->onDelete('cascade');

            $table->bigInteger('tv_id')->unsigned()->index();
            $table->foreign('tv_id')->references('id')->on('tv')->onDelete('cascade');

            $table->unique(['genre_id', 'tv_id']);
            $table->primary(['genre_id', 'tv_id']);
            $table->index(['genre_id', 'tv_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genre_tv');
    }
}
