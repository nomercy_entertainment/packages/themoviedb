<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Nomercy\Themoviedb\Models\Episode;
use Nomercy\Themoviedb\Models\Season;
use Nomercy\Themoviedb\Models\Tv;

class CreateGuestStarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_star', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->id();
            $table->string("name")->nullable();
            $table->integer("profile_id")->nullable();
            $table->mediumText("character")->nullable();
            $table->integer("adult")->default(0);
            $table->integer("cast_id")->nullable();
            $table->string("credit_id")->unique();
            $table->integer("gender")->nullable();
            $table->string("original_name")->nullable();
            $table->string("popularity")->nullable();
            $table->string("known_for_department")->nullable();
            $table->string("order")->nullable();
            $table->string("profile_path")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_star');
    }
}
