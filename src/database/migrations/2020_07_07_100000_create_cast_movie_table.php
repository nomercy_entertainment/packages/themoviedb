<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCastMovieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cast_movie', function (Blueprint $table) {
            $table->bigInteger('cast_id')->unsigned();
            $table->foreign('cast_id')->references('id')->on('cast')->onDelete('cascade');

            $table->bigInteger('movie_id')->unsigned()->index();
            $table->foreign('movie_id')->references('id')->on('movie')->onDelete('cascade');

            $table->unique(['cast_id', 'movie_id']);
            $table->primary(['cast_id', 'movie_id']);
            $table->index(['cast_id', 'movie_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cast_movie');
    }
}
