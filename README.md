# TheMoviedb API client for Laravel.

[![Latest Version on Packagist](https://img.shields.io/packagist/v/nomercy/themoviedb.svg?style=flat-square)](https://packagist.org/packages/nomercy/themoviedb)
[![Build Status](https://img.shields.io/travis/nomercy/themoviedb/master.svg?style=flat-square)](https://travis-ci.org/nomercy/themoviedb)
[![Quality Score](https://img.shields.io/scrutinizer/g/nomercy/themoviedb.svg?style=flat-square)](https://scrutinizer-ci.com/g/nomercy/themoviedb)
[![Total Downloads](https://img.shields.io/packagist/dt/nomercy/themoviedb.svg?style=flat-square)](https://packagist.org/packages/nomercy/themoviedb)

This packages is a API wrapper for [themoviedb.org](https://themoviedb.org)
It fetches all movie and tv show data with all related content.<br>
This includes all translations and automatically shows the translated content if available.<br>
You can choose if you want to download the images or load them directly from the api.<br><br>
This package is made specifically for the NoMercy MediaServer, pull request should not break depending clients.

## Installation

You can install the package via composer:

```bash
composer require nomercy/themoviedb
```

## Usage

``` php
// Usage description here
```
<!-- 
### Testing

``` bash
composer test
``` -->

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Warning

**This Package is in active development, as such this package may break at any time.<br>**
**Update with caution.**

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email stoney@nomercy.tv instead of using the issue tracker.

## Credits

- [Stoney Eagle](https://gitlab.nomercy.tv/Stoney_Eagle)
- [All Contributors](../../contributors)

## License

You may use and change this package for non commercial purpoces.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).




### Footnotes

<a name="myfootnote1">1</a>: Translations are made by users. if you mis something in your language, provide them at themoviedb.org
